package ru.hnau.jutils.possible

import ru.hnau.jutils.tryOrElse


class Possible<T : Any> private constructor(
        val data: T?,
        val error: Exception?
) {

    companion object {

        fun <T : Any> success(data: T): Possible<T> =
                Possible(data = data, error = null)

        fun success() = success(Unit)

        fun <T : Any> successOrUndefined(data: T?): Possible<T> =
                data?.let { success(it) } ?: undefined()

        fun <T : Any> successOrError(data: T?): Possible<T> =
                data?.let { success(it) } ?: error()

        fun <T : Any> error(error: Exception? = null): Possible<T> =
                Possible(data = null, error = error ?: Exception())

        fun <T : Any> error(message: String?): Possible<T> =
                error(Exception(message))

        fun <T : Any> errorOrUndefined(error: Exception?): Possible<T> =
                error?.let { error<T>(it) } ?: undefined()

        fun <T : Any> error(th: Throwable?): Possible<T> =
                (th as? Exception)?.let { Possible.error<T>(it) } ?: Possible.error(th?.message)

        fun <T : Any> errorOrUndefined(th: Throwable?): Possible<T> =
                (th as? Exception)?.let { Possible.errorOrUndefined<T>(it) }
                        ?: Possible.errorOrUndefined(th?.message)

        fun <T : Any> errorOrUndefined(message: String?): Possible<T> =
                message?.let { error<T>(it) } ?: undefined()

        fun <T : Any> undefined(): Possible<T> =
                Possible(data = null, error = null)

        inline fun <T : Any> trySuccessCatchError(throwsAction: () -> T): Possible<T> =
                trySuccessCatchErrorPossible { success(throwsAction.invoke()) }

        inline fun <T : Any> trySuccessCatchUndefined(throwsAction: () -> T): Possible<T> =
                trySuccessCatchUndefinedPossible { success(throwsAction.invoke()) }

        inline fun <T : Any> trySuccessCatchErrorPossible(throwsAction: () -> Possible<T>): Possible<T> =
                tryOrElse(
                        throwsAction = throwsAction,
                        onThrow = { Possible.error(it) }
                )

        inline fun <T : Any> trySuccessCatchUndefinedPossible(throwsAction: () -> Possible<T>): Possible<T> =
                tryOrElse(
                        throwsAction = throwsAction,
                        onThrow = { Possible.undefined() }
                )

    }

    operator fun component1() = data

    operator fun component2() = error

    override fun toString() = "Possible{" +
            "status=${handle("SUCCESS", "ERROR", "UNDEFINED")}" +
            (data?.let { ", data=$it" } ?: "") +
            (error?.let { ", error=$it" } ?: "") +
            "}"

    operator fun <O : Any> plus(other: Possible<O>) =
            mapPossible { thisData ->
                other.mapPossible { otherData ->
                    Possible.success(thisData to otherData)
                }
            }

    inline fun <R> ifSuccess(action: (T) -> R) =
            data?.let { action.invoke(it) }

    inline fun <R> ifError(action: (Exception) -> R) =
            error?.let { action.invoke(it) }

    inline fun <R> ifUndefined(action: () -> R) =
            if (data != null || error != null) null else action.invoke()

    inline fun <R> ifSuccessOrUndefined(action: (T?) -> R) =
            if (error != null) null else action.invoke(data)

    inline fun <R> ifSuccessOrError(action: (T?) -> R) =
            if (error == null && data == null) null else action.invoke(data)

    inline fun <R> ifErrorOrUndefined(action: (Exception?) -> R) =
            if (data != null) null else action.invoke(error)

    inline fun <R> handle(
            onSuccess: (T) -> R,
            onError: (Exception) -> R,
            onUndefined: () -> R
    ): R {

        if (data != null) {
            return onSuccess.invoke(data)
        }

        if (error != null) {
            return onError.invoke(error)
        }

        return onUndefined.invoke()
    }

    inline fun <R> handle(
            onSuccess: (T) -> R,
            onError: (Exception?) -> R
    ): R {

        if (data != null) {
            return onSuccess.invoke(data)
        }

        return onError.invoke(error)
    }

    fun <R> handle(
            forSuccess: R,
            forError: R,
            forUndefined: R
    ): R {
        if (data != null) {
            return forSuccess
        }

        if (error != null) {
            return forError
        }

        return forUndefined
    }

    fun <R> handle(
            forSuccess: R,
            forError: R
    ): R {
        if (data != null) {
            return forSuccess
        }

        return forError
    }

    fun getDataOrThrow() = handle(
            onSuccess = { it },
            onError = { throw (it ?: Exception()) }
    )

    inline fun getDataOrHandleError(
            onError: (ex: Exception) -> Unit,
            onUndefined: () -> Unit
    ) =
            handle(
                    onSuccess = { it },
                    onError = { onError.invoke(it); null },
                    onUndefined = { onUndefined.invoke(); null }
            )

    inline fun getDataOrHandleError(
            onError: (ex: Exception?) -> Unit
    ) =
            handle(
                    onSuccess = { it },
                    onError = { onError.invoke(it); null }
            )

    inline fun <O : Any> map(
            converter: (T) -> O
    ) =
            data?.let { success(converter.invoke(it)) } ?: error?.let { error<O>(it) }
            ?: undefined()

    inline fun <O : Any> tryMap(
            throwsConverter: (T) -> O
    ) =
            mapPossible { trySuccessCatchError { throwsConverter.invoke(it) } }

    inline fun <O : Any> mapPossible(
            converter: (T) -> Possible<O>
    ) =
            data?.let { converter.invoke(it) } ?: error?.let { error<O>(it) } ?: undefined()

   inline fun <O : Any> tryMapPossible(
            throwsConverter: (T) -> Possible<O>
    ) =
            mapPossible { data ->
                tryOrElse(
                        throwsAction = { throwsConverter.invoke(data) },
                        onThrow = { Possible.error(it) }
                )
            }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Possible<*>) return false

        if (data != null && data == other.data) return true
        if (error != null && error == other.error) return true
        if (data == null && other.data == null && error == null && other.error == null) return true

        return false
    }

    override fun hashCode(): Int {
        var result = data?.hashCode() ?: 0
        result = 31 * result + (error?.hashCode() ?: 0)
        return result
    }

}