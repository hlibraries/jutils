package ru.hnau.jutils.possible


fun <T : Any> ((Possible<T>) -> Unit).success(data: T) = invoke(Possible.success(data))

fun <T : Any> ((Possible<T>) -> Unit).successOrUndefined(data: T?) = invoke(Possible.successOrUndefined(data))

fun <T : Any> ((Possible<T>) -> Unit).successOrError(data: T?) = invoke(Possible.successOrError(data))

fun <T : Any> ((Possible<T>) -> Unit).error(error: Exception? = null) = invoke(Possible.error(error))

fun <T : Any> ((Possible<T>) -> Unit).error(message: String?) = invoke(Possible.error(message))

fun <T : Any> ((Possible<T>) -> Unit).errorOrUndefined(error: Exception?) = invoke(Possible.errorOrUndefined(error))

fun <T : Any> ((Possible<T>) -> Unit).errorOrUndefined(message: String?) = invoke(Possible.errorOrUndefined(message))

fun <T : Any> ((Possible<T>) -> Unit).undefined() = invoke(Possible.undefined())

fun ((Possible<Unit>) -> Unit).success() = invoke(Possible.success(Unit))

fun <T : Any> Iterable<Possible<T>>.listOfSuccess() = mapNotNull { it.data }

fun <T : Any> Iterable<Possible<T>>.listIfAllSuccess() =
        Possible.success(map { it.data ?: return Possible.error<List<T>>(it.error) })

fun <T : Any> List<Possible<T>>.onlySuccess() =
        mapNotNull { it.data }

fun <T : Any> List<Possible<T>>.allSuccess() =
        Possible.success(map {
            it.data ?: return Possible.error<List<T>>(it.error)
        })


fun <T : Any> List<Possible<T>>.firstSuccess(): Possible<T> {
    var firstError: Exception? = null
    forEach { possible ->
        val (data, error) = possible
        if (firstError == null && error != null) {
            firstError = error
        }
        if (data != null) {
            return possible
        }
    }
    return Possible.error(firstError)
}