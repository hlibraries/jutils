package ru.hnau.jutils

import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit


fun CountDownLatch.awaitOrThrow(time: TimeValue) = awaitOrElse(time) {
    throw IllegalStateException("CountDownLatch await time is up")
}

fun CountDownLatch.awaitOrElse(time: TimeValue, block: () -> Unit) {
    if (!await(time.milliseconds, TimeUnit.MILLISECONDS)) {
        block.invoke()
    }
}