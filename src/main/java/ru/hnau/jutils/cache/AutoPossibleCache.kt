package ru.hnau.jutils.cache

import ru.hnau.jutils.possible.Possible


open class AutoPossibleCache<K : Any, V : Any>(
        private val getter: (K) -> Possible<V>,
        capacity: Int
) : Cache<K, V>(
        capacity
) {

    operator fun get(key: K): Possible<V> = synchronized(this) {
        val existenceValue = getValue(key)
        if (existenceValue != null) {
            return@synchronized Possible.success(existenceValue)
        }
        val possibleValue = getter.invoke(key)
        val possibleData = possibleValue.data
        if (possibleData != null) {
            putValue(key, possibleData)
        }
        return@synchronized possibleValue
    }

}