package ru.hnau.jutils.cache

import ru.hnau.jutils.finisher.Finisher

@Deprecated("Use coroutines")
open class AsyncAutoCache<K : Any, V : Any>(
        private val getter: (K) -> Finisher<V>,
        capacity: Int
) : Cache<K, V>(
        capacity
) {

    operator fun get(key: K) = Finisher<V> { onFinished ->
        val existenceValue = getValue(key)
        if (existenceValue != null) {
            onFinished.invoke(existenceValue)
            return@Finisher
        }

        getter.invoke(key).await {value ->
            putValue(key, value)
            onFinished.invoke(value)
        }

    }

}