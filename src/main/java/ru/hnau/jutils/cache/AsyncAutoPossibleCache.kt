package ru.hnau.jutils.cache

import ru.hnau.jutils.finisher.Finisher
import ru.hnau.jutils.finisher.awaitError
import ru.hnau.jutils.possible.Possible


@Deprecated("Use coroutines")
open class AsyncAutoPossibleCache<K : Any, V : Any>(
        private val getter: (K) -> Finisher<Possible<V>>,
        capacity: Int
) : Cache<K, Finisher<Possible<V>>>(
        capacity
) {

    operator fun get(key: K) = getValue(key) ?: run {
        synchronized(this@AsyncAutoPossibleCache) {
            val finisher = getter.invoke(key)
            putValue(key, finisher)
            finisher.awaitError {
                removeValue(key)
            }
            return@synchronized finisher
        }
    }

}