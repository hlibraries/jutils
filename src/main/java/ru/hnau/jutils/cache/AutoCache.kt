package ru.hnau.jutils.cache


open class AutoCache<K : Any, V : Any>(
        private val getter: (K) -> V,
        capacity: Int
) : Cache<K, V>(
        capacity
) {

    fun getExistence(key: K) = getValue(key)

    operator fun get(key: K): V = synchronized(this) {
        var value = getValue(key)
        if (value != null) {
            return@synchronized value
        }
        value = getter.invoke(key)
        putValue(key, value)
        return@synchronized value
    }

}