package ru.hnau.jutils.cache


abstract class Cache<K : Any, V : Any>(
        capacity: Int
) {

    private val lruMap = LRUMap<K, V>(capacity)

    protected fun getValue(key: K) = lruMap[key]

    protected fun putValue(key: K, value: V) {
        lruMap[key] = value
    }

    protected fun removeValue(key: K) =
            lruMap.remove(key)

}