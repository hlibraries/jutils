package ru.hnau.jutils.cache

import java.util.LinkedHashMap

class LRUMap<K, V>(
        private val cacheSize: Int
) : LinkedHashMap<K, V>(
        16,
        0.75f,
        true
) {

    override fun removeEldestEntry(
            eldest: Map.Entry<K, V>?
    ) = size >= cacheSize

}
