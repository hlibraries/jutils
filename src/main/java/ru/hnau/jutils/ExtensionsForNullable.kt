package ru.hnau.jutils




inline fun <T, R> T?.handle(ifNotNull: (T) -> R, ifNull: () -> R) =
        if (this == null) ifNull.invoke() else ifNotNull.invoke(this)

fun <T, R> T?.handle(forNotNull: R, forNull: R) =
        if (this == null) forNull else forNotNull

inline fun <T, R> T?.ifNotNull(action: (T) -> R) = this?.let(action)

inline fun <T, R> T?.ifNull(action: () -> R) = if (this == null) action.invoke() else null

fun <T> ((Unit) -> T).invoke() = invoke(Unit)