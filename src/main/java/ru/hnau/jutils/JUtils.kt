package ru.hnau.jutils

import kotlin.math.abs


object JUtils {

    fun cutText(text: String, maxLength: Int): String {
        val maxTextLength = if (maxLength < 1) 1 else maxLength
        if (text.length <= maxTextLength) {
            return text
        }
        return text.substring(0..(maxTextLength - 1)) + '\u2026'
    }

    fun <R> getRussianCountSuffix(
            count: Int,
            forCount1: R,
            forCount2_4: R,
            forOther: R
    ) =
            abs(count).let { absCount ->
                when {
                    absCount in 11..19 -> forOther
                    absCount % 10 == 1 -> forCount1
                    absCount % 10 in 2..4 -> forCount2_4
                    else -> forOther
                }
            }

}