package ru.hnau.jutils.getter

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import ru.hnau.jutils.getter.base.GetterAsync
import ru.hnau.jutils.getter.base.GetterSync


open class SuspendGetter<P, V : Any>(
        private val getter: suspend (P) -> V
): GetterAsync<P, V> {

    companion object {

        fun <V : Any> simple(
                getter: suspend () -> V
        ) = SuspendGetter<Unit, V>(
                getter = { getter.invoke() }
        )

        fun <V : Any> simple(
                value: V
        ) =
                SuspendGetter.simple { value }

    }

    var existence: V? = null
        private set

    private val mutex = Mutex()

    override suspend fun get(param: P): V = mutex.withLock {
        var value = this.existence
        if (value == null) {
            value = getter.invoke(param)
            existence = value
        }
        return@withLock value
    }

}

fun <V : Any> V.toSuspendGetter() =
        SuspendGetter.simple(this)