package ru.hnau.jutils.getter

import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.getter.base.GetterSync
import ru.hnau.jutils.helpers.Outdatable
import ru.hnau.jutils.helpers.toOutdatable


open class MutableGetter<P, V : Any>(
        private val valueLifetime: TimeValue? = null,
        private val getter: (P) -> V
): GetterSync<P, V> {

    companion object {

        fun <V : Any> simple(
                valueLifetime: TimeValue? = null,
                getter: () -> V
        ) = MutableGetter<Unit, V>(
                valueLifetime = valueLifetime,
                getter = { getter.invoke() }
        )

        fun <V : Any> simple(
                value: V
        ) =
                MutableGetter.simple { value }

    }

    private var value: Outdatable<V>? = null

    val existence: V?
        get() = value?.value

    fun clear() {
        value = null
    }

    fun setValue(value: V) {
        this.value = value.toOutdatable(valueLifetime)
    }

    override fun get(param: P): V = synchronized(this) {
        var value = existence
        if (value == null) {
            value = getter.invoke(param)
            setValue(value)
        }
        return@synchronized value
    }

}

fun <V : Any> V.toMutableGetter() =
        MutableGetter.simple(this)