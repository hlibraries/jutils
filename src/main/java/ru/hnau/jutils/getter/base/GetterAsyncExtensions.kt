package ru.hnau.jutils.getter.base


fun <P, V : Any, R : Any> GetterAsync<P, V>.map(converter: (V) -> R) =
        object : GetterAsync<P, R> {
            override suspend fun get(param: P) =
                    converter.invoke(this@map.get(param))
        }

suspend fun <V : Any> GetterAsync<Unit, V>.get() = get(Unit)