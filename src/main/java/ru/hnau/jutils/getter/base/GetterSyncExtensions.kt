package ru.hnau.jutils.getter.base


fun <P, V : Any, R : Any> GetterSync<P, V>.map(converter: (V) -> R) =
        object : GetterSync<P, R> {
            override fun get(param: P) =
                    converter.invoke(this@map.get(param))
        }

fun <P, V : Any> GetterSync<P, V>.toAsync() =
        object : GetterAsync<P, V> {
            override suspend fun get(param: P) =
                    this@toAsync.get(param)
        }

fun <V : Any> GetterSync<Unit, V>.get() = get(Unit)