package ru.hnau.jutils.getter.base


interface GetterSync<P, V : Any> {

    fun get(param: P): V

}