package ru.hnau.jutils.getter.base


interface GetterAsync<P, V : Any> {

    suspend fun get(param: P): V

}