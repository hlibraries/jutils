package ru.hnau.jutils.getter

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.getter.base.GetterAsync
import ru.hnau.jutils.helpers.Outdatable
import ru.hnau.jutils.helpers.toOutdatable


open class SuspendMutableGetter<P, V : Any>(
        private val valueLifetime: TimeValue? = null,
        private val getter: suspend (P) -> V
) : GetterAsync<P, V> {

    companion object {

        fun <V : Any> simple(
                valueLifetime: TimeValue? = null,
                getter: suspend () -> V
        ) = SuspendMutableGetter<Unit, V>(
                valueLifetime = valueLifetime,
                getter = { getter.invoke() }
        )

        fun <V : Any> simple(
                value: V
        ) =
                SuspendMutableGetter.simple { value }

    }

    private var value: Outdatable<V>? = null

    val existence: V?
        get() = value?.value

    fun clear() {
        value = null
    }

    fun setValue(value: V) {
        this.value = value.toOutdatable(valueLifetime)
    }

    private val mutex = Mutex()

    override suspend fun get(param: P): V = mutex.withLock {
        var value = existence
        if (value == null) {
            value = getter.invoke(param)
            setValue(value)
        }
        return@withLock value
    }

}

fun <V : Any> V.toSuspendMutableGetter() =
        SuspendMutableGetter.simple(this)