package ru.hnau.jutils.getter

import ru.hnau.jutils.getter.base.GetterSync


open class Getter<P, V : Any>(
        private val getter: (P) -> V
) : GetterSync<P, V> {

    companion object {

        fun <V : Any> simple(
                getter: () -> V
        ) = Getter<Unit, V>(
                getter = { getter.invoke() }
        )

        fun <V : Any> simple(
                value: V
        ) =
                Companion.simple { value }

    }

    var existence: V? = null
        private set

    override fun get(param: P): V = synchronized(this) {
        var value = this.existence
        if (value == null) {
            value = getter.invoke(param)
            existence = value
        }
        return@synchronized value
    }

}

fun <V : Any> V.toGetter() =
        Getter.simple(this)