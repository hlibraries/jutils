package ru.hnau.jutils


fun <T : Comparable<T>> T.takeIfLargeThan(other: T) = takeIf { it > other }
fun <T : Comparable<T>> T.takeIfLessThan(other: T) = takeIf { it < other }
fun <T : Comparable<T>> T.takeIfNotLessThan(other: T) = takeIf { it >= other }
fun <T : Comparable<T>> T.takeIfNotLargeThan(other: T) = takeIf { it <= other }

fun Byte.takeIfPositive() = takeIf { it > 0 }
fun Byte.takeIfNegative() = takeIf { it < 0 }
fun Byte.takeIfNotNegative() = takeIf { it >= 0 }
fun Byte.takeIfNotPositive() = takeIf { it <= 0 }

fun Int.takeIfPositive() = takeIf { it > 0 }
fun Int.takeIfNegative() = takeIf { it < 0 }
fun Int.takeIfNotNegative() = takeIf { it >= 0 }
fun Int.takeIfNotPositive() = takeIf { it <= 0 }

fun Long.takeIfPositive() = takeIf { it > 0 }
fun Long.takeIfNegative() = takeIf { it < 0 }
fun Long.takeIfNotNegative() = takeIf { it >= 0 }
fun Long.takeIfNotPositive() = takeIf { it <= 0 }

fun Float.takeIfPositive() = takeIf { it > 0 }
fun Float.takeIfNegative() = takeIf { it < 0 }
fun Float.takeIfNotNegative() = takeIf { it >= 0 }
fun Float.takeIfNotPositive() = takeIf { it <= 0 }

fun Double.takeIfPositive() = takeIf { it > 0 }
fun Double.takeIfNegative() = takeIf { it < 0 }
fun Double.takeIfNotNegative() = takeIf { it >= 0 }
fun Double.takeIfNotPositive() = takeIf { it <= 0 }

fun <T, C: Collection<T>> C.takeIfEmpty() = takeIf(Collection<T>::isEmpty)
fun <T, C: Collection<T>> C.takeIfNotEmpty() = takeIf(Collection<T>::isNotEmpty)

fun <T> Array<T>.takeIfEmpty() = takeIf(Array<T>::isEmpty)
fun <T> Array<T>.takeIfNotEmpty() = takeIf(Array<T>::isNotEmpty)
fun IntArray.takeIfEmpty() = takeIf(IntArray::isEmpty)
fun IntArray.takeIfNotEmpty() = takeIf(IntArray::isNotEmpty)
fun FloatArray.takeIfEmpty() = takeIf(FloatArray::isEmpty)
fun FloatArray.takeIfNotEmpty() = takeIf(FloatArray::isNotEmpty)
fun DoubleArray.takeIfEmpty() = takeIf(DoubleArray::isEmpty)
fun DoubleArray.takeIfNotEmpty() = takeIf(DoubleArray::isNotEmpty)
fun ShortArray.takeIfEmpty() = takeIf(ShortArray::isEmpty)
fun ShortArray.takeIfNotEmpty() = takeIf(ShortArray::isNotEmpty)
fun ByteArray.takeIfEmpty() = takeIf(ByteArray::isEmpty)
fun ByteArray.takeIfNotEmpty() = takeIf(ByteArray::isNotEmpty)

fun String.takeIfEmpty() = takeIf(String::isEmpty)
fun String.takeIfNotEmpty() = takeIf(String::isNotEmpty)
fun String.takeIfBlank() = takeIf(String::isBlank)
fun String.takeIfNotBlank() = takeIf(String::isNotBlank)

fun Boolean.takeIfTrue() = if (this) this else null
fun Boolean.takeIfFalse() = if (!this) this else null