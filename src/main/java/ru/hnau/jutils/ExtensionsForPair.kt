package ru.hnau.jutils


inline fun <I, O> Pair<I, I>.map(converter: (I) -> O) =
        Pair(converter.invoke(first), converter.invoke(second))

inline fun <T> Pair<T, T>.forEach(action: (T) -> Unit) {
    action.invoke(first)
    action.invoke(second)
}