package ru.hnau.jutils

import ru.hnau.jutils.possible.Possible
import java.util.*


fun <T : Any> Optional<T>.getOrNull() =
        if (isPresent) get() else null

fun <T : Any> Optional<T>.toPossible() =
        Possible.successOrError(getOrNull())