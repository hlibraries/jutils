package ru.hnau.jutils.coroutines

import kotlinx.coroutines.*
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.possible.Possible
import kotlin.coroutines.CoroutineContext

fun <T> CoroutineContext.async(
        start: CoroutineStart = CoroutineStart.DEFAULT,
        block: suspend CoroutineScope.() -> T
) =
        GlobalScope.async(this, start, block)

fun CoroutineContext.launch(
        start: CoroutineStart = CoroutineStart.DEFAULT,
        block: suspend CoroutineScope.() -> Unit
) =
        GlobalScope.launch(this, start, block)

suspend fun delay(timeValue: TimeValue) =
        delay(timeValue.milliseconds)

@Throws(CancellationException::class)
suspend fun <T> withTimeout(timeValue: TimeValue, block: suspend CoroutineScope.() -> T) =
        withTimeout(timeValue.milliseconds, block)

suspend fun <T : Any> withTimeoutOrNull(timeValue: TimeValue, block: suspend CoroutineScope.() -> T) =
        withTimeoutOrNull(timeValue.milliseconds, block)

suspend fun <T : Any> withTimeoutOrPossibleError(timeValue: TimeValue, block: suspend CoroutineScope.() -> T) =
        Possible.trySuccessCatchError { withTimeout(timeValue, block) }

suspend fun <T : Any> withTimeoutOrError(timeValue: TimeValue, block: suspend CoroutineScope.() -> Possible<T>) =
        Possible.trySuccessCatchErrorPossible { withTimeout(timeValue, block) }

fun <T> T.toDeferred(): Deferred<T> =
        CompletableDeferred(this)