package ru.hnau.jutils.coroutines.deferred

import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred
import kotlin.coroutines.CoroutineContext


class LazyDeferred<T>(
        private val block: CompletableDeferred<T>.() -> Unit
) {

    private val deferred: Deferred<T> by lazy {
        deferred(block = block)
    }

    suspend fun await() =
            deferred.await()


}