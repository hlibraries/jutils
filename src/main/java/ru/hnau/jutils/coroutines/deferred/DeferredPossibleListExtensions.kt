package ru.hnau.jutils.coroutines.deferred

import kotlinx.coroutines.*
import ru.hnau.jutils.possible.*
import java.util.*
import kotlin.Any
import kotlin.Exception
import kotlin.synchronized


suspend fun <T : Any> List<Deferred<Possible<T>>>.awaitOnlySuccess() =
        awaitAll().mapNotNull { it.data }

fun <T : Any> List<Deferred<Possible<T>>>.allSuccess(parent: Job? = null): Deferred<Possible<List<T>>> {

    if (isEmpty()) {
        return deferred(Possible.success(emptyList()))
    }

    val result = LinkedList<T>()
    val handleAllResultsJob = Job()
    return deferred<Possible<List<T>>>(parent) {
        handleAllResults(handleAllResultsJob) { possibleResultOfDeferred ->
            possibleResultOfDeferred.handle(
                    onSuccess = { resultOfDeferred ->
                        synchronized(result) {
                            result.add(resultOfDeferred)
                            if (result.size >= size) {
                                complete(Possible.success(result))
                            }
                        }
                    },
                    onError = {
                        complete(Possible.error(it))
                    }
            )
        }
    }.apply { invokeOnCompletion { handleAllResultsJob.cancel() } }

}

fun <T : Any> List<Deferred<Possible<T>>>.firstSuccess(parent: Job? = null): Deferred<Possible<T>> {

    if (isEmpty()) {
        return deferred(Possible.undefined())
    }

    val locker = Object()
    var firstError: Exception? = null
    var errorsCount = 0
    val handleAllResultsJob = Job()

    return deferred<Possible<T>>(parent) {
        handleAllResults(handleAllResultsJob) { result ->
            result.handle(
                    onSuccess = { complete(Possible.success(it)) },
                    onError = { ex ->
                        synchronized(locker) {
                            firstError = firstError ?: ex
                            errorsCount++
                            if (errorsCount >= size) {
                                complete(Possible.error(firstError))
                            }
                        }
                    }
            )
        }
    }.apply { invokeOnCompletion { handleAllResultsJob.cancel() } }

}

suspend fun <T : Any> List<Deferred<Possible<List<T>>>>.awaitFlattenIfAllSuccess(): Possible<List<T>> =
        allSuccess().await().map { it.flatten() }

suspend fun <T : Any> List<Deferred<Possible<List<T>>>>.awaitFlattenOfSuccess(): List<T> =
        awaitOnlySuccess().flatten()