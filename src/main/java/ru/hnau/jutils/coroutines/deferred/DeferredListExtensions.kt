package ru.hnau.jutils.coroutines.deferred

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Job
import kotlinx.coroutines.awaitAll
import ru.hnau.jutils.coroutines.launch
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.possible.success
import kotlin.coroutines.CoroutineContext


suspend fun <T : Any> List<Deferred<List<T>>>.awaitFlatten() =
        awaitAll().flatten()

fun <T> List<Deferred<T>>.handleAllResults(
        handleAllResultsCoroutineContext: CoroutineContext,
        onCompleted: (T) -> Unit
) =
        forEach { deferred ->
            handleAllResultsCoroutineContext.launch {
                val deferredResult = deferred.await()
                onCompleted.invoke(deferredResult)
            }
        }

fun <T : Any> List<Deferred<T>>.first(parent: Job? = null): Deferred<Possible<T>> {

    if (isEmpty()) {
        return deferred(Possible.undefined())
    }

    val handleAllResultsJob = Job()
    return deferred(parent) {
        handleAllResults(handleAllResultsJob) {
            handleAllResultsJob.cancel()
            complete(Possible.success(it))
        }
    }
}