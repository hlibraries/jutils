package ru.hnau.jutils.coroutines.deferred

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.TimeoutCancellationException
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.coroutines.withTimeout

suspend fun <T, R> Deferred<T>.awaitWithTimeout(
        time: TimeValue,
        converter: suspend (T) -> R,
        timeoutAction: suspend (TimeoutCancellationException) -> R = { throw it }
) =
        try {
            withTimeout(time) { converter.invoke(await()) }
        } catch (ex: TimeoutCancellationException) {
            cancel()
            timeoutAction.invoke(ex)
        }

suspend fun <T> Deferred<T>.awaitWithTimeout(
        time: TimeValue,
        timeoutAction: suspend (TimeoutCancellationException) -> T = { throw it }
) = awaitWithTimeout(
        time = time,
        converter = { it },
        timeoutAction = timeoutAction
)