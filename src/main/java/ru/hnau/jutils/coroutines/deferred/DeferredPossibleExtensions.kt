package ru.hnau.jutils.coroutines.deferred

import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.possible.Possible


suspend fun <T : Any> Deferred<T>.awaitWithTimeoutOrPossibleError(
        time: TimeValue
) = awaitWithTimeout(
        time = time,
        converter = { Possible.success(it) },
        timeoutAction = { Possible.error(it) }
)

suspend fun <T : Any> Deferred<Possible<T>>.awaitWithTimeoutOrError(
        time: TimeValue
) = awaitWithTimeout(
        time = time,
        timeoutAction = { Possible.error(it) }
)

fun <T : Any> CompletableDeferred<Possible<T>>.success(data: T) =
        complete(Possible.success(data))

fun <T : Any> CompletableDeferred<Possible<T>>.successOrUndefined(data: T?) =
        complete(Possible.successOrUndefined(data))

fun <T : Any> CompletableDeferred<Possible<T>>.successOrError(data: T?) =
        complete(Possible.successOrError(data))

fun <T : Any> CompletableDeferred<Possible<T>>.error(error: Exception? = null) =
        complete(Possible.error(error))

fun <T : Any> CompletableDeferred<Possible<T>>.error(message: String?) =
        complete(Possible.error(message))

fun <T : Any> CompletableDeferred<Possible<T>>.errorOrUndefined(error: Exception? = null) =
        complete(Possible.errorOrUndefined(error))

fun <T : Any> CompletableDeferred<Possible<T>>.errorOrUndefined(message: String?) =
        complete(Possible.errorOrUndefined(message))

fun <T : Any> CompletableDeferred<Possible<T>>.undefined() =
        complete(Possible.undefined())

fun CompletableDeferred<Possible<Unit>>.success() =
        complete(Possible.success())