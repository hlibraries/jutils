package ru.hnau.jutils.coroutines.deferred

import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Job
import java.lang.Exception


@Suppress("EXPERIMENTAL_API_USAGE")
val <T : Any> Deferred<T>.resultOrNull: T?
    get() = synchronized(this) {
        return@synchronized if (isActive || isCancelled) null else getCompleted()
    }

val <T : Any> Deferred<T>.result: T
    get() = resultOrNull ?: throw IllegalStateException("Deferred has no result")

fun <T> deferred(
        parent: Job? = null,
        block: CompletableDeferred<T>.() -> Unit
): Deferred<T> =
        CompletableDeferred<T>(parent).apply(block)

fun <T> deferred(data: T): Deferred<T> = CompletableDeferred(data)

fun CompletableDeferred<Unit>.complete() = complete(Unit)
fun CompletableDeferred<Boolean>.completeTrue() = complete(true)
fun CompletableDeferred<Boolean>.completeFalse() = complete(false)
fun <T> CompletableDeferred<T>.cancel() = completeExceptionally(CancellationException())