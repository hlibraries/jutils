package ru.hnau.jutils.coroutines

import kotlinx.coroutines.CoroutineScope
import ru.hnau.jutils.coroutines.executor.InterruptableExecutor
import kotlin.collections.LinkedHashSet


class TasksFinalizer(
        private val interruptableExecutor: InterruptableExecutor
) {

    private val tasks =
            LinkedHashSet<suspend CoroutineScope.() -> Unit>()

    init {
        interruptableExecutor.onStartedProducer.attach {
            tasks.forEach(interruptableExecutor::invoke)
        }
    }

    fun finalize(
            task: suspend CoroutineScope.() -> Unit
    ) {
        lateinit var removableTask: suspend CoroutineScope.() -> Unit
        removableTask = {
            task.invoke(this)
            tasks.remove(removableTask)
        }
        tasks.add(removableTask)
        interruptableExecutor.invoke(removableTask)
    }

}