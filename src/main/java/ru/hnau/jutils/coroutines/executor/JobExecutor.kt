package ru.hnau.jutils.coroutines

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import ru.hnau.jutils.coroutines.executor.InterruptableExecutor
import ru.hnau.jutils.coroutines.launch
import ru.hnau.jutils.getter.MutableGetter
import ru.hnau.jutils.handle
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.SimpleProducer
import ru.hnau.jutils.producer.callListeners
import ru.hnau.jutils.tryOrElse
import java.lang.IllegalStateException
import kotlin.coroutines.CoroutineContext


open class JobExecutor(
        isActiveProducer: Producer<Boolean>,
        private val errorsHandler: (Throwable) -> Unit = DEFAULT_ERRORS_HANDLER
) : InterruptableExecutor(
        isActiveProducer = isActiveProducer
) {

    companion object {

        val DEFAULT_ERRORS_HANDLER: (Throwable) -> Unit = { error ->
            throw IllegalStateException("There is no real errors handler", error)
        }

    }

    private data class Executor(
            val cancel: () -> Unit,
            val context: CoroutineContext
    )

    private var executor: Executor? = null
        set(value) = synchronized(this) {
            field?.cancel?.invoke()
            field = value
        }

    protected open fun createCoroutineContext(job: Job): CoroutineContext = job

    override fun start() {
        val job = SupervisorJob()
        executor = Executor(
                cancel = job::cancel,
                context = createCoroutineContext(job)
        )
    }

    override fun stop() {
        executor = null
    }

    override fun invoke(
            coroutine: suspend CoroutineScope.() -> Unit
    ) {
        executor?.context?.launch {
            tryOrElse(
                    throwsAction = { coroutine.invoke(this) },
                    onThrow = errorsHandler
            )
        }
    }

}