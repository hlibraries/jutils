package ru.hnau.jutils.coroutines.executor

import kotlinx.coroutines.CoroutineScope
import ru.hnau.jutils.handle
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.SimpleProducer
import ru.hnau.jutils.producer.callListeners


abstract class InterruptableExecutor(
        isActiveProducer: Producer<Boolean>
) : (suspend CoroutineScope.() -> Unit) -> Unit {

    private val onStartedProducerInner = SimpleProducer<Unit>()
    val onStartedProducer: Producer<Unit>
        get() = onStartedProducerInner

    init {
        isActiveProducer.attach {
            it.handle(
                    onTrue = this::startInner,
                    onFalse = this::stop
            )
        }
    }

    protected fun startInner() {
        start()
        onStartedProducerInner.callListeners()
    }

    protected abstract fun start()

    protected abstract fun stop()


}