package ru.hnau.jutils.producer

import ru.hnau.jutils.producer.detacher.ProducerDetachers

@Deprecated("")
class ProducersContainer<T : Any>(
        private val producers: List<Producer<T>>
) : Producer<List<T>>() {

    private val values = producers.map { null as T? }.toMutableList()

    private val detachers = ProducerDetachers()

    private fun onDataFromProducerReceived(producerPos: Int, value: T) = synchronized(this) {
        val lastValue = values[producerPos]
        if (lastValue == value) {
            return@synchronized
        }
        values[producerPos] = value
        call(values.filterNotNull())
    }

    override fun onFirstAttached() {
        super.onFirstAttached()

        synchronized(this) {
            producers.forEachIndexed { i, producer ->
                producer.attach(detachers) { onDataFromProducerReceived(i, it) }
            }
        }
    }

    override fun onLastDetached() {
        super.onLastDetached()
        detachers.detachAllAndClear()
    }

}