package ru.hnau.jutils.producer

import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.possible.allSuccess
import ru.hnau.jutils.possible.firstSuccess
import ru.hnau.jutils.possible.onlySuccess
import ru.hnau.jutils.producer.extensions.flatten


fun <T : Any> Producer<Possible<T>>.attach(
        onSuccess: (T) -> Unit,
        onError: (Exception) -> Unit,
        onUndefined: () -> Unit
) =
        attach {
            it.handle(
                    onSuccess,
                    onError,
                    onUndefined
            )
        }

fun <T : Any> Producer<Possible<T>>.attach(
        onSuccess: (T) -> Unit,
        onError: (Exception?) -> Unit
) =
        attach {
            it.handle(
                    onSuccess,
                    onError
            )
        }

fun <T : Any, O : Any> Producer<Possible<T>>.mapPossible(
        converter: (T) -> O
) = map { it.map(converter) }

fun <T : Any, O : Any> Producer<Possible<T>>.mapToPossible(
        converter: (T) -> Possible<O>
) = map { it.mapPossible(converter) }

fun <T : Any, O : Any> Producer<Possible<T>>.tryMapPossible(
        throwsConverter: (T) -> O
) = map { it.tryMap(throwsConverter) }

fun <T : Any, O : Any> Producer<Possible<T>>.tryMapToPossible(
        throwsConverter: (T) -> Possible<O>
) = map { it.tryMapPossible(throwsConverter) }

fun <T : Any> Producer<Possible<T>>.filterSuccess() = mapNotNull { it.data }

fun <T : Any> Producer<Possible<T>>.filterError() = mapNotNull { it.error }

fun <T : Any> Producer<List<Possible<T>>>.onlySuccess() =
        map(List<Possible<T>>::onlySuccess)

fun <T : Any> Producer<List<Possible<T>>>.allSuccess() =
        map(List<Possible<T>>::allSuccess)

fun <T : Any> Producer<List<Possible<T>>>.firstSuccess() =
        map(List<Possible<T>>::firstSuccess)

fun <T : Any> Producer<List<Possible<List<T>>>>.flattenOnlySuccess() =
        onlySuccess().flatten()

fun <T : Any> Producer<List<Possible<List<T>>>>.flattenAllSuccess() =
        allSuccess().mapPossible(List<List<T>>::flatten)