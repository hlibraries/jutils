package ru.hnau.jutils.producer.combine

import ru.hnau.jutils.helpers.Box
import ru.hnau.jutils.helpers.toBox
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.StateProducer
import ru.hnau.jutils.producer.detacher.ProducerDetachers


abstract class CombineProducer3<P1, P2, P3, R : Any>(
        private val producer1: Producer<P1>,
        private val producer2: Producer<P2>,
        private val producer3: Producer<P3>
) : StateProducer<R>() {

    companion object {

        fun <P1, P2, P3, R : Any> create(
                producer1: Producer<P1>,
                producer2: Producer<P2>,
                producer3: Producer<P3>,
                combiner: (P1, P2, P3) -> R
        ): Producer<R> = object : CombineProducer3<P1, P2, P3, R>(
                producer1,
                producer2,
                producer3
        ) {

            override fun combine(param1: P1, param2: P2, param3: P3) =
                    combiner.invoke(param1, param2, param3)

        }

    }

    private var param1: Box<P1>? = null
        set(value) {
            field = value
            onParamReceived()
        }

    private var param2: Box<P2>? = null
        set(value) {
            field = value
            onParamReceived()
        }

    private var param3: Box<P3>? = null
        set(value) {
            field = value
            onParamReceived()
        }

    private val detachers = ProducerDetachers()

    private fun onParamReceived() = synchronized(this) {
        val param1 = this.param1 ?: return
        val param2 = this.param2 ?: return
        val param3 = this.param3 ?: return
        val result = combine(param1.value, param2.value, param3.value)
        update(result)
    }

    protected abstract fun combine(param1: P1, param2: P2, param3: P3): R

    override fun onFirstAttached() {
        super.onFirstAttached()
        producer1.attach(detachers) { param1 = it.toBox() }
        producer2.attach(detachers) { param2 = it.toBox() }
        producer3.attach(detachers) { param3 = it.toBox() }
    }

    override fun onLastDetached() {
        super.onLastDetached()
        detachers.detachAllAndClear()
    }

}