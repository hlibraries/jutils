package ru.hnau.jutils.producer


@Deprecated("")
class SimpleProducer<T>: Producer<T>() {

    fun callListeners(data: T) = call(data)

}

fun SimpleProducer<Unit>.callListeners() = callListeners(Unit)