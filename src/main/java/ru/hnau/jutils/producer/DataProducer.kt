package ru.hnau.jutils.producer


@Deprecated("Use ActualProducer")
open class DataProducer<T>(
        initialData: T
) : ActualProducer<T>(
        initialData
) {

    protected var data: T
        set(value) = update(value)
        get() = state

    protected fun call() = call(data)

    override fun toString() =
            "DataProducer{data=$data}"

}