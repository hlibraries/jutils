package ru.hnau.jutils.producer

import ru.hnau.jutils.producer.detacher.ProducerDetachers


class FunnelProducer<T>(
        private val producers: Iterable<Producer<T>>
) : Producer<T>() {

    private val detachers = ProducerDetachers()

    override fun onFirstAttached() {
        super.onFirstAttached()
        synchronized(producers) {
            producers.forEach { producer ->
                producer.attach(detachers, this::call)
            }
        }
    }

    override fun onLastDetached() {
        detachers.detachAllAndClear()
        super.onLastDetached()
    }


}