package ru.hnau.jutils.producer

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


class ActualProducerSimple<T>(
        initialState: T
) : ActualProducer<T>(
        initialState = initialState
), ReadWriteProperty<Any, T> {

    val currentState: T
        get() = state

    fun updateState(state: T) = update(state)
    fun updateStateIfChanged(state: T) = updateIfChanged(state)

    override operator fun getValue(thisRef: Any, property: KProperty<*>) =
            currentState

    override operator fun setValue(thisRef: Any, property: KProperty<*>, value: T) =
            updateStateIfChanged(value)

}

fun ActualProducerSimple<Boolean>.invert() =
        updateState(currentState.not())

