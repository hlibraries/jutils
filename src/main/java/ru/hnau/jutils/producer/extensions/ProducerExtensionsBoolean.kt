package ru.hnau.jutils.producer.extensions

import ru.hnau.jutils.*
import ru.hnau.jutils.producer.Producer


fun Producer<Boolean>.callIfTrue() = callIf { it }
fun Producer<Boolean?>.callIfTrueOrNull() = callIf { it != false }
fun Producer<Boolean>.callIfFalse() = callIf { !it }
fun Producer<Boolean?>.callIfFalseOrNull() = callIf { it != true }

operator fun Producer<Boolean>.not() = map(Boolean::not)

fun <T : Any> Producer<Boolean>.map(forTrue: T, forFalse: T) =
        map { it.handle(forTrue = forTrue, forFalse = forFalse) }

fun <T : Any> Producer<Boolean>.map(onTrue: () -> T, onFalse: () -> T) =
        map { it.handle(onTrue = onTrue, onFalse = onFalse) }

fun Producer<Boolean>.toInt() = map(Boolean::toInt)
fun Producer<Boolean>.toLong() = map(Boolean::toLong)
fun Producer<Boolean>.toByte() = map(Boolean::toByte)
fun Producer<Boolean>.toShort() = map(Boolean::toShort)
fun Producer<Boolean>.toFloat() = map(Boolean::toFloat)
fun Producer<Boolean>.toDouble() = map(Boolean::toDouble)