package ru.hnau.jutils.producer.extensions.byte

import ru.hnau.jutils.producer.Producer


operator fun Producer<Byte>.plus(other: Double) = map { it + other }
operator fun Producer<Byte>.minus(other: Double) = map { it - other }
operator fun Producer<Byte>.times(other: Double) = map { it * other }
operator fun Producer<Byte>.div(other: Double) = map { it / other }
operator fun Producer<Byte>.rem(other: Double) = map { it % other }

operator fun Producer<Byte>.plus(other: Producer<Double>) = combineWith(other, Byte::plus)
operator fun Producer<Byte>.minus(other: Producer<Double>) = combineWith(other, Byte::minus)
operator fun Producer<Byte>.times(other: Producer<Double>) = combineWith(other, Byte::times)
operator fun Producer<Byte>.div(other: Producer<Double>) = combineWith(other, Byte::div)
operator fun Producer<Byte>.rem(other: Producer<Double>) = combineWith(other, Byte::rem)