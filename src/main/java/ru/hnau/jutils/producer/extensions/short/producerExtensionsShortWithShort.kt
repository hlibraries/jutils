package ru.hnau.jutils.producer.extensions.short

import ru.hnau.jutils.producer.Producer


operator fun Producer<Short>.plus(other: Short) = map { it + other }
operator fun Producer<Short>.minus(other: Short) = map { it - other }
operator fun Producer<Short>.times(other: Short) = map { it * other }
operator fun Producer<Short>.div(other: Short) = map { it / other }
operator fun Producer<Short>.rem(other: Short) = map { it % other }

operator fun Producer<Short>.plus(other: Producer<Short>) = combineWith(other, Short::plus)
operator fun Producer<Short>.minus(other: Producer<Short>) = combineWith(other, Short::minus)
operator fun Producer<Short>.times(other: Producer<Short>) = combineWith(other, Short::times)
operator fun Producer<Short>.div(other: Producer<Short>) = combineWith(other, Short::div)
operator fun Producer<Short>.rem(other: Producer<Short>) = combineWith(other, Short::rem)