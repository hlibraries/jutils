package ru.hnau.jutils.producer.extensions.int

import ru.hnau.jutils.producer.Producer


operator fun Producer<Int>.plus(other: Float) = map { it + other }
operator fun Producer<Int>.minus(other: Float) = map { it - other }
operator fun Producer<Int>.times(other: Float) = map { it * other }
operator fun Producer<Int>.div(other: Float) = map { it / other }
operator fun Producer<Int>.rem(other: Float) = map { it % other }

operator fun Producer<Int>.plus(other: Producer<Float>) = combineWith(other, Int::plus)
operator fun Producer<Int>.minus(other: Producer<Float>) = combineWith(other, Int::minus)
operator fun Producer<Int>.times(other: Producer<Float>) = combineWith(other, Int::times)
operator fun Producer<Int>.div(other: Producer<Float>) = combineWith(other, Int::div)
operator fun Producer<Int>.rem(other: Producer<Float>) = combineWith(other, Int::rem)