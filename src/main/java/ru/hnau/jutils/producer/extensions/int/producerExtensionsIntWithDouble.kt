package ru.hnau.jutils.producer.extensions.int

import ru.hnau.jutils.producer.Producer


operator fun Producer<Int>.plus(other: Double) = map { it + other }
operator fun Producer<Int>.minus(other: Double) = map { it - other }
operator fun Producer<Int>.times(other: Double) = map { it * other }
operator fun Producer<Int>.div(other: Double) = map { it / other }
operator fun Producer<Int>.rem(other: Double) = map { it % other }

operator fun Producer<Int>.plus(other: Producer<Double>) = combineWith(other, Int::plus)
operator fun Producer<Int>.minus(other: Producer<Double>) = combineWith(other, Int::minus)
operator fun Producer<Int>.times(other: Producer<Double>) = combineWith(other, Int::times)
operator fun Producer<Int>.div(other: Producer<Double>) = combineWith(other, Int::div)
operator fun Producer<Int>.rem(other: Producer<Double>) = combineWith(other, Int::rem)