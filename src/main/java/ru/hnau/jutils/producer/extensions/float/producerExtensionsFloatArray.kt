package ru.hnau.jutils.producer.extensions.float

import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.filter


fun Producer<FloatArray>.callIfEmpty() = callIf { it.isEmpty() }
fun Producer<FloatArray?>.callIfNullOrEmpty() = callIf { it?.isEmpty() != false }
fun Producer<FloatArray>.filterNotEmpty() = filter { it.isNotEmpty() }
fun Producer<FloatArray>.mapIsEmpty() = map { it.isEmpty() }