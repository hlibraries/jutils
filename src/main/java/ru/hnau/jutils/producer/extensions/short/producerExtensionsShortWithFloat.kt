package ru.hnau.jutils.producer.extensions.short

import ru.hnau.jutils.producer.Producer


operator fun Producer<Short>.plus(other: Float) = map { it + other }
operator fun Producer<Short>.minus(other: Float) = map { it - other }
operator fun Producer<Short>.times(other: Float) = map { it * other }
operator fun Producer<Short>.div(other: Float) = map { it / other }
operator fun Producer<Short>.rem(other: Float) = map { it % other }

operator fun Producer<Short>.plus(other: Producer<Float>) = combineWith(other, Short::plus)
operator fun Producer<Short>.minus(other: Producer<Float>) = combineWith(other, Short::minus)
operator fun Producer<Short>.times(other: Producer<Float>) = combineWith(other, Short::times)
operator fun Producer<Short>.div(other: Producer<Float>) = combineWith(other, Short::div)
operator fun Producer<Short>.rem(other: Producer<Float>) = combineWith(other, Short::rem)