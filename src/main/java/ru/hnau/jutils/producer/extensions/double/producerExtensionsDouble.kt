package ru.hnau.jutils.producer.extensions.double

import jdk.nashorn.internal.runtime.JSType.toBoolean
import ru.hnau.jutils.*
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.filter


fun Producer<Double>.callIfZero() = callIf { it == 0.0 }
fun Producer<Double>.filterPositive() = filter { it > 0 }
fun Producer<Double>.filterNegative() = filter { it < 0 }
fun Producer<Double>.filterNotPositive() = filter { it <= 0 }
fun Producer<Double>.filterNotNegative() = filter { it >= 0 }

fun Producer<Double>.coerceAtLeast(minimumValue: Double) =
        map { it.coerceAtLeast(minimumValue) }

fun Producer<Double>.coerceAtMost(maximumValue: Double) =
        map { it.coerceAtMost(maximumValue) }

fun Producer<Double>.coerceIn(range: ClosedFloatingPointRange<Double>) =
        map { it.coerceIn(range) }

fun Producer<Double>.coerceIn(range: ClosedRange<Double>) =
        map { it.coerceIn(range) }

fun Producer<Double>.coerceIn(minimumValue: Double, maximumValue: Double) =
        map { it.coerceIn(minimumValue, maximumValue) }

fun Producer<Double>.toBoolean() = map(Double::toBoolean)
fun Producer<Double>.toInt() = map(Double::toInt)
fun Producer<Double>.toLong() = map(Double::toLong)
fun Producer<Double>.toByte() = map(Double::toByte)
fun Producer<Double>.toShort() = map(Double::toShort)
fun Producer<Double>.toFloat() = map(Double::toFloat)
fun Producer<Double>.toDouble() = map(Double::toDouble)

operator fun Producer<Double>.unaryPlus() = map(Double::unaryPlus)
operator fun Producer<Double>.unaryMinus() = map(Double::unaryMinus)
operator fun Producer<Double>.inc() = map(Double::inc)