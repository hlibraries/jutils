package ru.hnau.jutils.producer.extensions.float

import ru.hnau.jutils.producer.Producer


operator fun Producer<Float>.plus(other: Float) = map { it + other }
operator fun Producer<Float>.minus(other: Float) = map { it - other }
operator fun Producer<Float>.times(other: Float) = map { it * other }
operator fun Producer<Float>.div(other: Float) = map { it / other }
operator fun Producer<Float>.rem(other: Float) = map { it % other }

operator fun Producer<Float>.plus(other: Producer<Float>) = combineWith(other, Float::plus)
operator fun Producer<Float>.minus(other: Producer<Float>) = combineWith(other, Float::minus)
operator fun Producer<Float>.times(other: Producer<Float>) = combineWith(other, Float::times)
operator fun Producer<Float>.div(other: Producer<Float>) = combineWith(other, Float::div)
operator fun Producer<Float>.rem(other: Producer<Float>) = combineWith(other, Float::rem)