package ru.hnau.jutils.producer.extensions.int

import ru.hnau.jutils.producer.Producer


operator fun Producer<Int>.plus(other: Int) = map { it + other }
operator fun Producer<Int>.minus(other: Int) = map { it - other }
operator fun Producer<Int>.times(other: Int) = map { it * other }
operator fun Producer<Int>.div(other: Int) = map { it / other }
operator fun Producer<Int>.rem(other: Int) = map { it % other }

operator fun Producer<Int>.plus(other: Producer<Int>) = combineWith(other, Int::plus)
operator fun Producer<Int>.minus(other: Producer<Int>) = combineWith(other, Int::minus)
operator fun Producer<Int>.times(other: Producer<Int>) = combineWith(other, Int::times)
operator fun Producer<Int>.div(other: Producer<Int>) = combineWith(other, Int::div)
operator fun Producer<Int>.rem(other: Producer<Int>) = combineWith(other, Int::rem)