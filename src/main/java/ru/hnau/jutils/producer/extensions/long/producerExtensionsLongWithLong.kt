package ru.hnau.jutils.producer.extensions.long

import ru.hnau.jutils.producer.Producer


operator fun Producer<Long>.plus(other: Long) = map { it + other }
operator fun Producer<Long>.minus(other: Long) = map { it - other }
operator fun Producer<Long>.times(other: Long) = map { it * other }
operator fun Producer<Long>.div(other: Long) = map { it / other }
operator fun Producer<Long>.rem(other: Long) = map { it % other }

operator fun Producer<Long>.plus(other: Producer<Long>) = combineWith(other, Long::plus)
operator fun Producer<Long>.minus(other: Producer<Long>) = combineWith(other, Long::minus)
operator fun Producer<Long>.times(other: Producer<Long>) = combineWith(other, Long::times)
operator fun Producer<Long>.div(other: Producer<Long>) = combineWith(other, Long::div)
operator fun Producer<Long>.rem(other: Producer<Long>) = combineWith(other, Long::rem)