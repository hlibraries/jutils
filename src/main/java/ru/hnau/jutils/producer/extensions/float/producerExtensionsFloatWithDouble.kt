package ru.hnau.jutils.producer.extensions.float

import ru.hnau.jutils.producer.Producer


operator fun Producer<Float>.plus(other: Double) = map { it + other }
operator fun Producer<Float>.minus(other: Double) = map { it - other }
operator fun Producer<Float>.times(other: Double) = map { it * other }
operator fun Producer<Float>.div(other: Double) = map { it / other }
operator fun Producer<Float>.rem(other: Double) = map { it % other }

operator fun Producer<Float>.plus(other: Producer<Double>) = combineWith(other, Float::plus)
operator fun Producer<Float>.minus(other: Producer<Double>) = combineWith(other, Float::minus)
operator fun Producer<Float>.times(other: Producer<Double>) = combineWith(other, Float::times)
operator fun Producer<Float>.div(other: Producer<Double>) = combineWith(other, Float::div)
operator fun Producer<Float>.rem(other: Producer<Double>) = combineWith(other, Float::rem)