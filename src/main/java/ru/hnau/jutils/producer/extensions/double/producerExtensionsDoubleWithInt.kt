package ru.hnau.jutils.producer.extensions.double

import ru.hnau.jutils.producer.Producer


operator fun Producer<Double>.plus(other: Int) = map { it + other }
operator fun Producer<Double>.minus(other: Int) = map { it - other }
operator fun Producer<Double>.times(other: Int) = map { it * other }
operator fun Producer<Double>.div(other: Int) = map { it / other }
operator fun Producer<Double>.rem(other: Int) = map { it % other }

operator fun Producer<Double>.plus(other: Producer<Int>) = combineWith(other, Double::plus)
operator fun Producer<Double>.minus(other: Producer<Int>) = combineWith(other, Double::minus)
operator fun Producer<Double>.times(other: Producer<Int>) = combineWith(other, Double::times)
operator fun Producer<Double>.div(other: Producer<Int>) = combineWith(other, Double::div)
operator fun Producer<Double>.rem(other: Producer<Int>) = combineWith(other, Double::rem)