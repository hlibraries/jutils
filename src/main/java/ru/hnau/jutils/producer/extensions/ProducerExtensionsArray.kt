package ru.hnau.jutils.producer.extensions

import ru.hnau.jutils.producer.Producer


fun <T> Producer<Array<T>>.callIfEmpty() = callIf { it.isEmpty() }
fun <T> Producer<Array<T>?>.callIfNullOrEmpty() = callIf { it.isNullOrEmpty() }
fun <T> Producer<Array<T>>.filterNotEmpty() = filter { it.isNotEmpty() }
fun <T> Producer<Array<T>>.mapIsEmpty() = map { it.isEmpty() }