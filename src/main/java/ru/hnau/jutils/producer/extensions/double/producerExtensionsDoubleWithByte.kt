package ru.hnau.jutils.producer.extensions.double

import ru.hnau.jutils.producer.Producer


operator fun Producer<Double>.plus(other: Byte) = map { it + other }
operator fun Producer<Double>.minus(other: Byte) = map { it - other }
operator fun Producer<Double>.times(other: Byte) = map { it * other }
operator fun Producer<Double>.div(other: Byte) = map { it / other }
operator fun Producer<Double>.rem(other: Byte) = map { it % other }

operator fun Producer<Double>.plus(other: Producer<Byte>) = combineWith(other, Double::plus)
operator fun Producer<Double>.minus(other: Producer<Byte>) = combineWith(other, Double::minus)
operator fun Producer<Double>.times(other: Producer<Byte>) = combineWith(other, Double::times)
operator fun Producer<Double>.div(other: Producer<Byte>) = combineWith(other, Double::div)
operator fun Producer<Double>.rem(other: Producer<Byte>) = combineWith(other, Double::rem)