package ru.hnau.jutils.producer.extensions.double

import ru.hnau.jutils.producer.Producer


operator fun Producer<Double>.plus(other: Float) = map { it + other }
operator fun Producer<Double>.minus(other: Float) = map { it - other }
operator fun Producer<Double>.times(other: Float) = map { it * other }
operator fun Producer<Double>.div(other: Float) = map { it / other }
operator fun Producer<Double>.rem(other: Float) = map { it % other }

operator fun Producer<Double>.plus(other: Producer<Float>) = combineWith(other, Double::plus)
operator fun Producer<Double>.minus(other: Producer<Float>) = combineWith(other, Double::minus)
operator fun Producer<Double>.times(other: Producer<Float>) = combineWith(other, Double::times)
operator fun Producer<Double>.div(other: Producer<Float>) = combineWith(other, Double::div)
operator fun Producer<Double>.rem(other: Producer<Float>) = combineWith(other, Double::rem)