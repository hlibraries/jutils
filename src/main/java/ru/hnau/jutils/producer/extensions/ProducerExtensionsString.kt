package ru.hnau.jutils.producer.extensions

import ru.hnau.jutils.producer.Producer
import java.util.*


fun Producer<String>.callIfEmpty() = callIf { it.isEmpty() }
fun Producer<String>.callIfBlank() = callIf { it.isBlank() }
fun Producer<String?>.callIfNullOrEmpty() = callIf { it.isNullOrEmpty() }
fun Producer<String?>.callIfNullOrBlank() = callIf { it.isNullOrBlank() }

fun Producer<String>.filterNotEmpty() = filter { it.isNotEmpty() }
fun Producer<String>.filterNotBlank() = filter { it.isNotBlank() }

fun Producer<String>.toUpperCase() = map { it.toUpperCase() }
fun Producer<String>.toLowerCase() = map { it.toLowerCase() }
fun Producer<String>.toUpperCase(locale: Locale) = map { it.toUpperCase(locale) }
fun Producer<String>.toLowerCase(locale: Locale) = map { it.toLowerCase(locale) }
fun Producer<String>.trim() = map { it.trim() }
fun Producer<String>.trimStart() = map { it.trimStart() }
fun Producer<String>.trimEnd() = map { it.trimEnd() }