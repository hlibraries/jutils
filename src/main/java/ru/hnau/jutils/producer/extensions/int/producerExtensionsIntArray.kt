package ru.hnau.jutils.producer.extensions.int

import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.filter


fun Producer<IntArray>.callIfEmpty() = callIf { it.isEmpty() }
fun Producer<IntArray?>.callIfNullOrEmpty() = callIf { it?.isEmpty() != false }
fun Producer<IntArray>.filterNotEmpty() = filter { it.isNotEmpty() }
fun Producer<IntArray>.mapIsEmpty() = map { it.isEmpty() }