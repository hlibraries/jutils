package ru.hnau.jutils.producer.extensions.short

import ru.hnau.jutils.*
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.filter


fun Producer<Short>.callIfZero() = callIf { it.toInt() == 0 }
fun Producer<Short>.filterPositive() = filter { it > 0 }
fun Producer<Short>.filterNegative() = filter { it < 0 }
fun Producer<Short>.filterNotPositive() = filter { it <= 0 }
fun Producer<Short>.filterNotNegative() = filter { it >= 0 }

fun Producer<Short>.coerceAtLeast(minimumValue: Short) =
        map { it.coerceAtLeast(minimumValue) }

fun Producer<Short>.coerceAtMost(maximumValue: Short) =
        map { it.coerceAtMost(maximumValue) }

fun Producer<Short>.coerceIn(range: ClosedFloatingPointRange<Short>) =
        map { it.coerceIn(range) }

fun Producer<Short>.coerceIn(range: ClosedRange<Short>) =
        map { it.coerceIn(range) }

fun Producer<Short>.coerceIn(minimumValue: Short, maximumValue: Short) =
        map { it.coerceIn(minimumValue, maximumValue) }

fun Producer<Short>.toBoolean() = map(Short::toBoolean)
fun Producer<Short>.toInt() = map(Short::toInt)
fun Producer<Short>.toLong() = map(Short::toLong)
fun Producer<Short>.toByte() = map(Short::toByte)
fun Producer<Short>.toShort() = map(Short::toShort)
fun Producer<Short>.toFloat() = map(Short::toFloat)
fun Producer<Short>.toDouble() = map(Short::toDouble)

operator fun Producer<Short>.unaryPlus() = map(Short::unaryPlus)
operator fun Producer<Short>.unaryMinus() = map(Short::unaryMinus)
operator fun Producer<Short>.inc() = map(Short::inc)