package ru.hnau.jutils.producer.extensions.long

import ru.hnau.jutils.producer.Producer


operator fun Producer<Long>.plus(other: Byte) = map { it + other }
operator fun Producer<Long>.minus(other: Byte) = map { it - other }
operator fun Producer<Long>.times(other: Byte) = map { it * other }
operator fun Producer<Long>.div(other: Byte) = map { it / other }
operator fun Producer<Long>.rem(other: Byte) = map { it % other }

operator fun Producer<Long>.plus(other: Producer<Byte>) = combineWith(other, Long::plus)
operator fun Producer<Long>.minus(other: Producer<Byte>) = combineWith(other, Long::minus)
operator fun Producer<Long>.times(other: Producer<Byte>) = combineWith(other, Long::times)
operator fun Producer<Long>.div(other: Producer<Byte>) = combineWith(other, Long::div)
operator fun Producer<Long>.rem(other: Producer<Byte>) = combineWith(other, Long::rem)