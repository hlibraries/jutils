package ru.hnau.jutils.producer.extensions.byte

import ru.hnau.jutils.*
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.filter


fun Producer<Byte>.callIfZero() = callIf { it.toInt() == 0 }
fun Producer<Byte>.filterPositive() = filter { it > 0 }
fun Producer<Byte>.filterNegative() = filter { it < 0 }
fun Producer<Byte>.filterNotPositive() = filter { it <= 0 }
fun Producer<Byte>.filterNotNegative() = filter { it >= 0 }

fun Producer<Byte>.coerceAtLeast(minimumValue: Byte) =
        map { it.coerceAtLeast(minimumValue) }

fun Producer<Byte>.coerceAtMost(maximumValue: Byte) =
        map { it.coerceAtMost(maximumValue) }

fun Producer<Byte>.coerceIn(range: ClosedFloatingPointRange<Byte>) =
        map { it.coerceIn(range) }

fun Producer<Byte>.coerceIn(range: ClosedRange<Byte>) =
        map { it.coerceIn(range) }

fun Producer<Byte>.coerceIn(minimumValue: Byte, maximumValue: Byte) =
        map { it.coerceIn(minimumValue, maximumValue) }

fun Producer<Byte>.toBoolean() = map(Byte::toBoolean)
fun Producer<Byte>.toInt() = map(Byte::toInt)
fun Producer<Byte>.toLong() = map(Byte::toLong)
fun Producer<Byte>.toByte() = map(Byte::toByte)
fun Producer<Byte>.toShort() = map(Byte::toShort)
fun Producer<Byte>.toFloat() = map(Byte::toFloat)
fun Producer<Byte>.toDouble() = map(Byte::toDouble)

operator fun Producer<Byte>.unaryPlus() = map(Byte::unaryPlus)
operator fun Producer<Byte>.unaryMinus() = map(Byte::unaryMinus)
operator fun Producer<Byte>.inc() = map(Byte::inc)