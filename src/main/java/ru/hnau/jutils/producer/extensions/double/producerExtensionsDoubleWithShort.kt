package ru.hnau.jutils.producer.extensions.double

import ru.hnau.jutils.producer.Producer


operator fun Producer<Double>.plus(other: Short) = map { it + other }
operator fun Producer<Double>.minus(other: Short) = map { it - other }
operator fun Producer<Double>.times(other: Short) = map { it * other }
operator fun Producer<Double>.div(other: Short) = map { it / other }
operator fun Producer<Double>.rem(other: Short) = map { it % other }

operator fun Producer<Double>.plus(other: Producer<Short>) = combineWith(other, Double::plus)
operator fun Producer<Double>.minus(other: Producer<Short>) = combineWith(other, Double::minus)
operator fun Producer<Double>.times(other: Producer<Short>) = combineWith(other, Double::times)
operator fun Producer<Double>.div(other: Producer<Short>) = combineWith(other, Double::div)
operator fun Producer<Double>.rem(other: Producer<Short>) = combineWith(other, Double::rem)