package ru.hnau.jutils.producer.extensions.short

import ru.hnau.jutils.producer.Producer


operator fun Producer<Short>.plus(other: Double) = map { it + other }
operator fun Producer<Short>.minus(other: Double) = map { it - other }
operator fun Producer<Short>.times(other: Double) = map { it * other }
operator fun Producer<Short>.div(other: Double) = map { it / other }
operator fun Producer<Short>.rem(other: Double) = map { it % other }

operator fun Producer<Short>.plus(other: Producer<Double>) = combineWith(other, Short::plus)
operator fun Producer<Short>.minus(other: Producer<Double>) = combineWith(other, Short::minus)
operator fun Producer<Short>.times(other: Producer<Double>) = combineWith(other, Short::times)
operator fun Producer<Short>.div(other: Producer<Double>) = combineWith(other, Short::div)
operator fun Producer<Short>.rem(other: Producer<Double>) = combineWith(other, Short::rem)