package ru.hnau.jutils.producer.extensions.float

import ru.hnau.jutils.producer.Producer


operator fun Producer<Float>.plus(other: Short) = map { it + other }
operator fun Producer<Float>.minus(other: Short) = map { it - other }
operator fun Producer<Float>.times(other: Short) = map { it * other }
operator fun Producer<Float>.div(other: Short) = map { it / other }
operator fun Producer<Float>.rem(other: Short) = map { it % other }

operator fun Producer<Float>.plus(other: Producer<Short>) = combineWith(other, Float::plus)
operator fun Producer<Float>.minus(other: Producer<Short>) = combineWith(other, Float::minus)
operator fun Producer<Float>.times(other: Producer<Short>) = combineWith(other, Float::times)
operator fun Producer<Float>.div(other: Producer<Short>) = combineWith(other, Float::div)
operator fun Producer<Float>.rem(other: Producer<Short>) = combineWith(other, Float::rem)