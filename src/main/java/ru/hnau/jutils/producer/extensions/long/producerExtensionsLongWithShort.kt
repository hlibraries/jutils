package ru.hnau.jutils.producer.extensions.long

import ru.hnau.jutils.producer.Producer


operator fun Producer<Long>.plus(other: Short) = map { it + other }
operator fun Producer<Long>.minus(other: Short) = map { it - other }
operator fun Producer<Long>.times(other: Short) = map { it * other }
operator fun Producer<Long>.div(other: Short) = map { it / other }
operator fun Producer<Long>.rem(other: Short) = map { it % other }

operator fun Producer<Long>.plus(other: Producer<Short>) = combineWith(other, Long::plus)
operator fun Producer<Long>.minus(other: Producer<Short>) = combineWith(other, Long::minus)
operator fun Producer<Long>.times(other: Producer<Short>) = combineWith(other, Long::times)
operator fun Producer<Long>.div(other: Producer<Short>) = combineWith(other, Long::div)
operator fun Producer<Long>.rem(other: Producer<Short>) = combineWith(other, Long::rem)