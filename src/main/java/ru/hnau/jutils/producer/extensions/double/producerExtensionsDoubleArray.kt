package ru.hnau.jutils.producer.extensions.double

import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.filter


fun Producer<DoubleArray>.callIfEmpty() = callIf { it.isEmpty() }
fun Producer<DoubleArray?>.callIfNullOrEmpty() = callIf { it?.isEmpty() != false }
fun Producer<DoubleArray>.filterNotEmpty() = filter { it.isNotEmpty() }
fun Producer<DoubleArray>.mapIsEmpty() = map { it.isEmpty() }