package ru.hnau.jutils.producer.extensions.float

import ru.hnau.jutils.producer.Producer


operator fun Producer<Float>.plus(other: Long) = map { it + other }
operator fun Producer<Float>.minus(other: Long) = map { it - other }
operator fun Producer<Float>.times(other: Long) = map { it * other }
operator fun Producer<Float>.div(other: Long) = map { it / other }
operator fun Producer<Float>.rem(other: Long) = map { it % other }

operator fun Producer<Float>.plus(other: Producer<Long>) = combineWith(other, Float::plus)
operator fun Producer<Float>.minus(other: Producer<Long>) = combineWith(other, Float::minus)
operator fun Producer<Float>.times(other: Producer<Long>) = combineWith(other, Float::times)
operator fun Producer<Float>.div(other: Producer<Long>) = combineWith(other, Float::div)
operator fun Producer<Float>.rem(other: Producer<Long>) = combineWith(other, Float::rem)