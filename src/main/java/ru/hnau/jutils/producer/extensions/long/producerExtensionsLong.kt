package ru.hnau.jutils.producer.extensions.long

import ru.hnau.jutils.*
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.filter


fun Producer<Long>.callIfZero() = callIf { it == 0L }
fun Producer<Long>.filterPositive() = filter { it > 0 }
fun Producer<Long>.filterNegative() = filter { it < 0 }
fun Producer<Long>.filterNotPositive() = filter { it <= 0 }
fun Producer<Long>.filterNotNegative() = filter { it >= 0 }

fun Producer<Long>.coerceAtLeast(minimumValue: Long) =
        map { it.coerceAtLeast(minimumValue) }

fun Producer<Long>.coerceAtMost(maximumValue: Long) =
        map { it.coerceAtMost(maximumValue) }

fun Producer<Long>.coerceIn(range: ClosedFloatingPointRange<Long>) =
        map { it.coerceIn(range) }

fun Producer<Long>.coerceIn(range: ClosedRange<Long>) =
        map { it.coerceIn(range) }

fun Producer<Long>.coerceIn(minimumValue: Long, maximumValue: Long) =
        map { it.coerceIn(minimumValue, maximumValue) }

fun Producer<Long>.toBoolean() = map(Long::toBoolean)
fun Producer<Long>.toInt() = map(Long::toInt)
fun Producer<Long>.toLong() = map(Long::toLong)
fun Producer<Long>.toByte() = map(Long::toByte)
fun Producer<Long>.toShort() = map(Long::toShort)
fun Producer<Long>.toFloat() = map(Long::toFloat)
fun Producer<Long>.toDouble() = map(Long::toDouble)

operator fun Producer<Long>.unaryPlus() = map(Long::unaryPlus)
operator fun Producer<Long>.unaryMinus() = map(Long::unaryMinus)
operator fun Producer<Long>.inc() = map(Long::inc)