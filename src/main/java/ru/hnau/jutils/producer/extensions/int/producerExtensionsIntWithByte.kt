package ru.hnau.jutils.producer.extensions.int

import ru.hnau.jutils.producer.Producer


operator fun Producer<Int>.plus(other: Byte) = map { it + other }
operator fun Producer<Int>.minus(other: Byte) = map { it - other }
operator fun Producer<Int>.times(other: Byte) = map { it * other }
operator fun Producer<Int>.div(other: Byte) = map { it / other }
operator fun Producer<Int>.rem(other: Byte) = map { it % other }

operator fun Producer<Int>.plus(other: Producer<Byte>) = combineWith(other, Int::plus)
operator fun Producer<Int>.minus(other: Producer<Byte>) = combineWith(other, Int::minus)
operator fun Producer<Int>.times(other: Producer<Byte>) = combineWith(other, Int::times)
operator fun Producer<Int>.div(other: Producer<Byte>) = combineWith(other, Int::div)
operator fun Producer<Int>.rem(other: Producer<Byte>) = combineWith(other, Int::rem)