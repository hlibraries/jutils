package ru.hnau.jutils.producer.extensions.float

import ru.hnau.jutils.producer.Producer


operator fun Producer<Float>.plus(other: Int) = map { it + other }
operator fun Producer<Float>.minus(other: Int) = map { it - other }
operator fun Producer<Float>.times(other: Int) = map { it * other }
operator fun Producer<Float>.div(other: Int) = map { it / other }
operator fun Producer<Float>.rem(other: Int) = map { it % other }

operator fun Producer<Float>.plus(other: Producer<Int>) = combineWith(other, Float::plus)
operator fun Producer<Float>.minus(other: Producer<Int>) = combineWith(other, Float::minus)
operator fun Producer<Float>.times(other: Producer<Int>) = combineWith(other, Float::times)
operator fun Producer<Float>.div(other: Producer<Int>) = combineWith(other, Float::div)
operator fun Producer<Float>.rem(other: Producer<Int>) = combineWith(other, Float::rem)