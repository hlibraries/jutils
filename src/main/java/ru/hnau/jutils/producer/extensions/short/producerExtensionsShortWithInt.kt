package ru.hnau.jutils.producer.extensions.short

import ru.hnau.jutils.producer.Producer


operator fun Producer<Short>.plus(other: Int) = map { it + other }
operator fun Producer<Short>.minus(other: Int) = map { it - other }
operator fun Producer<Short>.times(other: Int) = map { it * other }
operator fun Producer<Short>.div(other: Int) = map { it / other }
operator fun Producer<Short>.rem(other: Int) = map { it % other }

operator fun Producer<Short>.plus(other: Producer<Int>) = combineWith(other, Short::plus)
operator fun Producer<Short>.minus(other: Producer<Int>) = combineWith(other, Short::minus)
operator fun Producer<Short>.times(other: Producer<Int>) = combineWith(other, Short::times)
operator fun Producer<Short>.div(other: Producer<Int>) = combineWith(other, Short::div)
operator fun Producer<Short>.rem(other: Producer<Int>) = combineWith(other, Short::rem)