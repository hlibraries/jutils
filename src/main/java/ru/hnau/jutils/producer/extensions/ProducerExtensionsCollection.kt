package ru.hnau.jutils.producer.extensions

import ru.hnau.jutils.producer.Producer


fun <T, C: Collection<T>> Producer<C>.callIfEmpty() = callIf { it.isEmpty() }
fun <T, C: Collection<T>> Producer<C?>.callIfNullOrEmpty() = callIf { it.isNullOrEmpty() }
fun <T, C: Collection<T>> Producer<C>.filterNotEmpty() = filter { it.isNotEmpty() }
fun <T, C: Collection<T>> Producer<C>.mapIsEmpty() = map { it.isEmpty() }