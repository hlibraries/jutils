package ru.hnau.jutils.producer.extensions.double

import ru.hnau.jutils.producer.Producer


operator fun Producer<Double>.plus(other: Long) = map { it + other }
operator fun Producer<Double>.minus(other: Long) = map { it - other }
operator fun Producer<Double>.times(other: Long) = map { it * other }
operator fun Producer<Double>.div(other: Long) = map { it / other }
operator fun Producer<Double>.rem(other: Long) = map { it % other }

operator fun Producer<Double>.plus(other: Producer<Long>) = combineWith(other, Double::plus)
operator fun Producer<Double>.minus(other: Producer<Long>) = combineWith(other, Double::minus)
operator fun Producer<Double>.times(other: Producer<Long>) = combineWith(other, Double::times)
operator fun Producer<Double>.div(other: Producer<Long>) = combineWith(other, Double::div)
operator fun Producer<Double>.rem(other: Producer<Long>) = combineWith(other, Double::rem)