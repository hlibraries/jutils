package ru.hnau.jutils.producer.extensions.float

import ru.hnau.jutils.*
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.filter


fun Producer<Float>.callIfZero() = callIf { it == 0f }
fun Producer<Float>.filterPositive() = filter { it > 0 }
fun Producer<Float>.filterNegative() = filter { it < 0 }
fun Producer<Float>.filterNotPositive() = filter { it <= 0 }
fun Producer<Float>.filterNotNegative() = filter { it >= 0 }

fun Producer<Float>.coerceAtLeast(minimumValue: Float) =
        map { it.coerceAtLeast(minimumValue) }

fun Producer<Float>.coerceAtMost(maximumValue: Float) =
        map { it.coerceAtMost(maximumValue) }

fun Producer<Float>.coerceIn(range: ClosedFloatingPointRange<Float>) =
        map { it.coerceIn(range) }

fun Producer<Float>.coerceIn(range: ClosedRange<Float>) =
        map { it.coerceIn(range) }

fun Producer<Float>.coerceIn(minimumValue: Float, maximumValue: Float) =
        map { it.coerceIn(minimumValue, maximumValue) }

fun Producer<Float>.toBoolean() = map(Float::toBoolean)
fun Producer<Float>.toInt() = map(Float::toInt)
fun Producer<Float>.toLong() = map(Float::toLong)
fun Producer<Float>.toByte() = map(Float::toByte)
fun Producer<Float>.toShort() = map(Float::toShort)
fun Producer<Float>.toFloat() = map(Float::toFloat)
fun Producer<Float>.toDouble() = map(Float::toDouble)

operator fun Producer<Float>.unaryPlus() = map(Float::unaryPlus)
operator fun Producer<Float>.unaryMinus() = map(Float::unaryMinus)
operator fun Producer<Float>.inc() = map(Float::inc)