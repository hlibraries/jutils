package ru.hnau.jutils.producer.extensions.float

import ru.hnau.jutils.producer.Producer


operator fun Producer<Float>.plus(other: Byte) = map { it + other }
operator fun Producer<Float>.minus(other: Byte) = map { it - other }
operator fun Producer<Float>.times(other: Byte) = map { it * other }
operator fun Producer<Float>.div(other: Byte) = map { it / other }
operator fun Producer<Float>.rem(other: Byte) = map { it % other }

operator fun Producer<Float>.plus(other: Producer<Byte>) = combineWith(other, Float::plus)
operator fun Producer<Float>.minus(other: Producer<Byte>) = combineWith(other, Float::minus)
operator fun Producer<Float>.times(other: Producer<Byte>) = combineWith(other, Float::times)
operator fun Producer<Float>.div(other: Producer<Byte>) = combineWith(other, Float::div)
operator fun Producer<Float>.rem(other: Producer<Byte>) = combineWith(other, Float::rem)