package ru.hnau.jutils.producer.extensions.byte

import ru.hnau.jutils.producer.Producer


operator fun Producer<Byte>.plus(other: Long) = map { it + other }
operator fun Producer<Byte>.minus(other: Long) = map { it - other }
operator fun Producer<Byte>.times(other: Long) = map { it * other }
operator fun Producer<Byte>.div(other: Long) = map { it / other }
operator fun Producer<Byte>.rem(other: Long) = map { it % other }

operator fun Producer<Byte>.plus(other: Producer<Long>) = combineWith(other, Byte::plus)
operator fun Producer<Byte>.minus(other: Producer<Long>) = combineWith(other, Byte::minus)
operator fun Producer<Byte>.times(other: Producer<Long>) = combineWith(other, Byte::times)
operator fun Producer<Byte>.div(other: Producer<Long>) = combineWith(other, Byte::div)
operator fun Producer<Byte>.rem(other: Producer<Long>) = combineWith(other, Byte::rem)