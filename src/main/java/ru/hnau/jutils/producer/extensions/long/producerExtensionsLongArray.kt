package ru.hnau.jutils.producer.extensions.long

import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.filter


fun Producer<LongArray>.callIfEmpty() = callIf { it.isEmpty() }
fun Producer<LongArray?>.callIfNullOrEmpty() = callIf { it?.isEmpty() != false }
fun Producer<LongArray>.filterNotEmpty() = filter { it.isNotEmpty() }
fun Producer<LongArray>.mapIsEmpty() = map { it.isEmpty() }