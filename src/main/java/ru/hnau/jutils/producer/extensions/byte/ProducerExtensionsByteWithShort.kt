package ru.hnau.jutils.producer.extensions.byte

import ru.hnau.jutils.producer.Producer


operator fun Producer<Byte>.plus(other: Short) = map { it + other }
operator fun Producer<Byte>.minus(other: Short) = map { it - other }
operator fun Producer<Byte>.times(other: Short) = map { it * other }
operator fun Producer<Byte>.div(other: Short) = map { it / other }
operator fun Producer<Byte>.rem(other: Short) = map { it % other }

operator fun Producer<Byte>.plus(other: Producer<Short>) = combineWith(other, Byte::plus)
operator fun Producer<Byte>.minus(other: Producer<Short>) = combineWith(other, Byte::minus)
operator fun Producer<Byte>.times(other: Producer<Short>) = combineWith(other, Byte::times)
operator fun Producer<Byte>.div(other: Producer<Short>) = combineWith(other, Byte::div)
operator fun Producer<Byte>.rem(other: Producer<Short>) = combineWith(other, Byte::rem)