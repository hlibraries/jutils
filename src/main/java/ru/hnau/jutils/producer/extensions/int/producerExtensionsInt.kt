package ru.hnau.jutils.producer.extensions.int

import ru.hnau.jutils.*
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.filter


fun Producer<Int>.callIfZero() = callIf { it == 0 }
fun Producer<Int>.filterPositive() = filter { it > 0 }
fun Producer<Int>.filterNegative() = filter { it < 0 }
fun Producer<Int>.filterNotPositive() = filter { it <= 0 }
fun Producer<Int>.filterNotNegative() = filter { it >= 0 }

fun Producer<Int>.coerceAtLeast(minimumValue: Int) =
        map { it.coerceAtLeast(minimumValue) }

fun Producer<Int>.coerceAtMost(maximumValue: Int) =
        map { it.coerceAtMost(maximumValue) }

fun Producer<Int>.coerceIn(range: ClosedFloatingPointRange<Int>) =
        map { it.coerceIn(range) }

fun Producer<Int>.coerceIn(range: ClosedRange<Int>) =
        map { it.coerceIn(range) }

fun Producer<Int>.coerceIn(minimumValue: Int, maximumValue: Int) =
        map { it.coerceIn(minimumValue, maximumValue) }

fun Producer<Int>.toBoolean() = map(Int::toBoolean)
fun Producer<Int>.toInt() = map(Int::toInt)
fun Producer<Int>.toLong() = map(Int::toLong)
fun Producer<Int>.toByte() = map(Int::toByte)
fun Producer<Int>.toShort() = map(Int::toShort)
fun Producer<Int>.toFloat() = map(Int::toFloat)
fun Producer<Int>.toDouble() = map(Int::toDouble)

operator fun Producer<Int>.unaryPlus() = map(Int::unaryPlus)
operator fun Producer<Int>.unaryMinus() = map(Int::unaryMinus)
operator fun Producer<Int>.inc() = map(Int::inc)