package ru.hnau.jutils.producer.extensions

import ru.hnau.jutils.producer.Producer


fun <T : Any> Producer<T?>.callIfNull() =
        callIf { it == null }

fun <T : Any> Producer<T?>.callIfNotNull() =
        callIf { it != null }