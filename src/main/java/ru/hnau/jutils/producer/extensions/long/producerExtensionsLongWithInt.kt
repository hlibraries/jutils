package ru.hnau.jutils.producer.extensions.long

import ru.hnau.jutils.producer.Producer


operator fun Producer<Long>.plus(other: Int) = map { it + other }
operator fun Producer<Long>.minus(other: Int) = map { it - other }
operator fun Producer<Long>.times(other: Int) = map { it * other }
operator fun Producer<Long>.div(other: Int) = map { it / other }
operator fun Producer<Long>.rem(other: Int) = map { it % other }

operator fun Producer<Long>.plus(other: Producer<Int>) = combineWith(other, Long::plus)
operator fun Producer<Long>.minus(other: Producer<Int>) = combineWith(other, Long::minus)
operator fun Producer<Long>.times(other: Producer<Int>) = combineWith(other, Long::times)
operator fun Producer<Long>.div(other: Producer<Int>) = combineWith(other, Long::div)
operator fun Producer<Long>.rem(other: Producer<Int>) = combineWith(other, Long::rem)