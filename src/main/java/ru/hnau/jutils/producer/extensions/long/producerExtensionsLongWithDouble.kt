package ru.hnau.jutils.producer.extensions.long

import ru.hnau.jutils.producer.Producer


operator fun Producer<Long>.plus(other: Double) = map { it + other }
operator fun Producer<Long>.minus(other: Double) = map { it - other }
operator fun Producer<Long>.times(other: Double) = map { it * other }
operator fun Producer<Long>.div(other: Double) = map { it / other }
operator fun Producer<Long>.rem(other: Double) = map { it % other }

operator fun Producer<Long>.plus(other: Producer<Double>) = combineWith(other, Long::plus)
operator fun Producer<Long>.minus(other: Producer<Double>) = combineWith(other, Long::minus)
operator fun Producer<Long>.times(other: Producer<Double>) = combineWith(other, Long::times)
operator fun Producer<Long>.div(other: Producer<Double>) = combineWith(other, Long::div)
operator fun Producer<Long>.rem(other: Producer<Double>) = combineWith(other, Long::rem)