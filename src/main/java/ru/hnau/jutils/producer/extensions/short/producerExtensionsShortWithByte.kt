package ru.hnau.jutils.producer.extensions.short

import ru.hnau.jutils.producer.Producer


operator fun Producer<Short>.plus(other: Byte) = map { it + other }
operator fun Producer<Short>.minus(other: Byte) = map { it - other }
operator fun Producer<Short>.times(other: Byte) = map { it * other }
operator fun Producer<Short>.div(other: Byte) = map { it / other }
operator fun Producer<Short>.rem(other: Byte) = map { it % other }

operator fun Producer<Short>.plus(other: Producer<Byte>) = combineWith(other, Short::plus)
operator fun Producer<Short>.minus(other: Producer<Byte>) = combineWith(other, Short::minus)
operator fun Producer<Short>.times(other: Producer<Byte>) = combineWith(other, Short::times)
operator fun Producer<Short>.div(other: Producer<Byte>) = combineWith(other, Short::div)
operator fun Producer<Short>.rem(other: Producer<Byte>) = combineWith(other, Short::rem)