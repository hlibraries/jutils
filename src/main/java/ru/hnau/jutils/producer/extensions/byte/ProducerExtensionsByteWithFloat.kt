package ru.hnau.jutils.producer.extensions.byte

import ru.hnau.jutils.producer.Producer


operator fun Producer<Byte>.plus(other: Float) = map { it + other }
operator fun Producer<Byte>.minus(other: Float) = map { it - other }
operator fun Producer<Byte>.times(other: Float) = map { it * other }
operator fun Producer<Byte>.div(other: Float) = map { it / other }
operator fun Producer<Byte>.rem(other: Float) = map { it % other }

operator fun Producer<Byte>.plus(other: Producer<Float>) = combineWith(other, Byte::plus)
operator fun Producer<Byte>.minus(other: Producer<Float>) = combineWith(other, Byte::minus)
operator fun Producer<Byte>.times(other: Producer<Float>) = combineWith(other, Byte::times)
operator fun Producer<Byte>.div(other: Producer<Float>) = combineWith(other, Byte::div)
operator fun Producer<Byte>.rem(other: Producer<Float>) = combineWith(other, Byte::rem)