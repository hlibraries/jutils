package ru.hnau.jutils.producer.extensions.double

import ru.hnau.jutils.producer.Producer


operator fun Producer<Double>.plus(other: Double) = map { it + other }
operator fun Producer<Double>.minus(other: Double) = map { it - other }
operator fun Producer<Double>.times(other: Double) = map { it * other }
operator fun Producer<Double>.div(other: Double) = map { it / other }
operator fun Producer<Double>.rem(other: Double) = map { it % other }

operator fun Producer<Double>.plus(other: Producer<Double>) = combineWith(other, Double::plus)
operator fun Producer<Double>.minus(other: Producer<Double>) = combineWith(other, Double::minus)
operator fun Producer<Double>.times(other: Producer<Double>) = combineWith(other, Double::times)
operator fun Producer<Double>.div(other: Producer<Double>) = combineWith(other, Double::div)
operator fun Producer<Double>.rem(other: Producer<Double>) = combineWith(other, Double::rem)