package ru.hnau.jutils.producer.extensions.byte

import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.filter


fun Producer<ByteArray>.callIfEmpty() = callIf { it.isEmpty() }
fun Producer<ByteArray?>.callIfNullOrEmpty() = callIf { it?.isEmpty() != false }
fun Producer<ByteArray>.filterNotEmpty() = filter { it.isNotEmpty() }
fun Producer<ByteArray>.mapIsEmpty() = map { it.isEmpty() }