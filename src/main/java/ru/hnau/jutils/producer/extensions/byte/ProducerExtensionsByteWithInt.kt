package ru.hnau.jutils.producer.extensions.byte

import ru.hnau.jutils.producer.Producer


operator fun Producer<Byte>.plus(other: Int) = map { it + other }
operator fun Producer<Byte>.minus(other: Int) = map { it - other }
operator fun Producer<Byte>.times(other: Int) = map { it * other }
operator fun Producer<Byte>.div(other: Int) = map { it / other }
operator fun Producer<Byte>.rem(other: Int) = map { it % other }

operator fun Producer<Byte>.plus(other: Producer<Int>) = combineWith(other, Byte::plus)
operator fun Producer<Byte>.minus(other: Producer<Int>) = combineWith(other, Byte::minus)
operator fun Producer<Byte>.times(other: Producer<Int>) = combineWith(other, Byte::times)
operator fun Producer<Byte>.div(other: Producer<Int>) = combineWith(other, Byte::div)
operator fun Producer<Byte>.rem(other: Producer<Int>) = combineWith(other, Byte::rem)