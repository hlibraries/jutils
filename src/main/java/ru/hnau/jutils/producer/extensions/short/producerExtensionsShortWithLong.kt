package ru.hnau.jutils.producer.extensions.short

import ru.hnau.jutils.producer.Producer


operator fun Producer<Short>.plus(other: Long) = map { it + other }
operator fun Producer<Short>.minus(other: Long) = map { it - other }
operator fun Producer<Short>.times(other: Long) = map { it * other }
operator fun Producer<Short>.div(other: Long) = map { it / other }
operator fun Producer<Short>.rem(other: Long) = map { it % other }

operator fun Producer<Short>.plus(other: Producer<Long>) = combineWith(other, Short::plus)
operator fun Producer<Short>.minus(other: Producer<Long>) = combineWith(other, Short::minus)
operator fun Producer<Short>.times(other: Producer<Long>) = combineWith(other, Short::times)
operator fun Producer<Short>.div(other: Producer<Long>) = combineWith(other, Short::div)
operator fun Producer<Short>.rem(other: Producer<Long>) = combineWith(other, Short::rem)