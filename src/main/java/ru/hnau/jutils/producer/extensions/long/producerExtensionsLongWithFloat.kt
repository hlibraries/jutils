package ru.hnau.jutils.producer.extensions.long

import ru.hnau.jutils.producer.Producer


operator fun Producer<Long>.plus(other: Float) = map { it + other }
operator fun Producer<Long>.minus(other: Float) = map { it - other }
operator fun Producer<Long>.times(other: Float) = map { it * other }
operator fun Producer<Long>.div(other: Float) = map { it / other }
operator fun Producer<Long>.rem(other: Float) = map { it % other }

operator fun Producer<Long>.plus(other: Producer<Float>) = combineWith(other, Long::plus)
operator fun Producer<Long>.minus(other: Producer<Float>) = combineWith(other, Long::minus)
operator fun Producer<Long>.times(other: Producer<Float>) = combineWith(other, Long::times)
operator fun Producer<Long>.div(other: Producer<Float>) = combineWith(other, Long::div)
operator fun Producer<Long>.rem(other: Producer<Float>) = combineWith(other, Long::rem)