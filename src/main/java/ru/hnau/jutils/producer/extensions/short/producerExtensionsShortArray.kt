package ru.hnau.jutils.producer.extensions.short

import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.filter


fun Producer<ShortArray>.callIfEmpty() = callIf { it.isEmpty() }
fun Producer<ShortArray?>.callIfNullOrEmpty() = callIf { it?.isEmpty() != false }
fun Producer<ShortArray>.filterNotEmpty() = filter { it.isNotEmpty() }
fun Producer<ShortArray>.mapIsEmpty() = map { it.isEmpty() }