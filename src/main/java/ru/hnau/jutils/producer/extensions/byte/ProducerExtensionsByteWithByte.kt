package ru.hnau.jutils.producer.extensions.byte

import ru.hnau.jutils.producer.Producer


operator fun Producer<Byte>.plus(other: Byte) = map { it + other }
operator fun Producer<Byte>.minus(other: Byte) = map { it - other }
operator fun Producer<Byte>.times(other: Byte) = map { it * other }
operator fun Producer<Byte>.div(other: Byte) = map { it / other }
operator fun Producer<Byte>.rem(other: Byte) = map { it % other }

operator fun Producer<Byte>.plus(other: Producer<Byte>) = combineWith(other, Byte::plus)
operator fun Producer<Byte>.minus(other: Producer<Byte>) = combineWith(other, Byte::minus)
operator fun Producer<Byte>.times(other: Producer<Byte>) = combineWith(other, Byte::times)
operator fun Producer<Byte>.div(other: Producer<Byte>) = combineWith(other, Byte::div)
operator fun Producer<Byte>.rem(other: Producer<Byte>) = combineWith(other, Byte::rem)