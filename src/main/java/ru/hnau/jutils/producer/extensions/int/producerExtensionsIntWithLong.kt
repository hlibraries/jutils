package ru.hnau.jutils.producer.extensions.int

import ru.hnau.jutils.producer.Producer


operator fun Producer<Int>.plus(other: Long) = map { it + other }
operator fun Producer<Int>.minus(other: Long) = map { it - other }
operator fun Producer<Int>.times(other: Long) = map { it * other }
operator fun Producer<Int>.div(other: Long) = map { it / other }
operator fun Producer<Int>.rem(other: Long) = map { it % other }

operator fun Producer<Int>.plus(other: Producer<Long>) = combineWith(other, Int::plus)
operator fun Producer<Int>.minus(other: Producer<Long>) = combineWith(other, Int::minus)
operator fun Producer<Int>.times(other: Producer<Long>) = combineWith(other, Int::times)
operator fun Producer<Int>.div(other: Producer<Long>) = combineWith(other, Int::div)
operator fun Producer<Int>.rem(other: Producer<Long>) = combineWith(other, Int::rem)