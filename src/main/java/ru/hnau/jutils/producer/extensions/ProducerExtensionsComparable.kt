package ru.hnau.jutils.producer.extensions

import ru.hnau.jutils.producer.Producer


fun <T : Comparable<T>> Producer<T>.filterLargeThan(value: T) =
        filter { it > value }

fun <T : Comparable<T>> Producer<T>.filterLessThan(value: T) =
        filter { it < value }

fun <T : Comparable<T>> Producer<T>.filterNotLessThan(value: T) =
        filter { it >= value }

fun <T : Comparable<T>> Producer<T>.filterNotLargeThan(value: T) =
        filter { it <= value }

fun <T : Comparable<T>> Producer<T>.coerceAtLeast(minimumValue: T) =
        map { it.coerceAtLeast(minimumValue) }

fun <T : Comparable<T>> Producer<T>.coerceAtMost(maximumValue: T) =
        map { it.coerceAtMost(maximumValue) }

fun <T : Comparable<T>> Producer<T>.coerceIn(range: ClosedFloatingPointRange<T>) =
        map { it.coerceIn(range) }

fun <T : Comparable<T>> Producer<T>.coerceIn(range: ClosedRange<T>) =
        map { it.coerceIn(range) }

fun <T : Comparable<T>> Producer<T>.coerceIn(minimumValue: T, maximumValue: T) =
        map { it.coerceIn(minimumValue, maximumValue) }