package ru.hnau.jutils.producer.extensions.int

import ru.hnau.jutils.producer.Producer


operator fun Producer<Int>.plus(other: Short) = map { it + other }
operator fun Producer<Int>.minus(other: Short) = map { it - other }
operator fun Producer<Int>.times(other: Short) = map { it * other }
operator fun Producer<Int>.div(other: Short) = map { it / other }
operator fun Producer<Int>.rem(other: Short) = map { it % other }

operator fun Producer<Int>.plus(other: Producer<Short>) = combineWith(other, Int::plus)
operator fun Producer<Int>.minus(other: Producer<Short>) = combineWith(other, Int::minus)
operator fun Producer<Int>.times(other: Producer<Short>) = combineWith(other, Int::times)
operator fun Producer<Int>.div(other: Producer<Short>) = combineWith(other, Int::div)
operator fun Producer<Int>.rem(other: Producer<Short>) = combineWith(other, Int::rem)