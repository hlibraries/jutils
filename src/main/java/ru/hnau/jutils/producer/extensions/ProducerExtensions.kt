package ru.hnau.jutils.producer.extensions

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Collections.combine
import ru.hnau.jutils.handle
import ru.hnau.jutils.producer.*
import ru.hnau.jutils.producer.combine.CombineProducer
import ru.hnau.jutils.producer.combine.CombineProducer3

fun <T> Producer<List<List<T>>>.flatten() =
        map(List<List<T>>::flatten)

fun <T> T.toProducer() =
        AlwaysProducer.create(this)

fun <T> (() -> T).toProducer() =
        AlwaysProducer.create(this)

fun <T : Any> Producer<Producer<T>>.collapse(): Producer<T> =
        ObservingProducer(this)

fun <T> Producer.Companion.empty() =
        object : Producer<T>() {}


fun <T : Any> Producer<T>.filter(predicate: (T) -> Boolean) =
        wrap<T> { data ->
            if (predicate.invoke(data)) {
                call(data)
            }
        }

fun <P1, P2, R : Any> Producer.Companion.combine(
        producer1: Producer<P1>,
        producer2: Producer<P2>,
        combiner: (P1, P2) -> R
) = CombineProducer.create(
        producer1,
        producer2,
        combiner
)

fun <P1, P2, P3, R : Any> Producer.Companion.combine(
        producer1: Producer<P1>,
        producer2: Producer<P2>,
        producer3: Producer<P3>,
        combiner: (P1, P2, P3) -> R
) = CombineProducer3.create(
        producer1,
        producer2,
        producer3,
        combiner
)

fun <T : Any> Producer<T?>.filterNotNull() =
        mapNotNull { it }

fun <T> Iterable<Producer<T>>.funnel(): Producer<T> =
        FunnelProducer(this)

fun <T : Any> Producer<T>.observeWhen(whenSign: Producer<Boolean>): Producer<T> {
    val emptyProducer = Producer.empty<T>()
    return whenSign.map { isNeedObserve ->
        isNeedObserve.handle(
                forTrue = this@observeWhen,
                forFalse = emptyProducer
        )
    }.collapse()
}

fun <T : Any> Producer<T>.filterUnique(): Producer<T> {
    var oldValue: T? = null
    return wrap<T> { value ->
        if (oldValue == value) {
            return@wrap
        }
        oldValue = value
        call(value)
    }
}

fun <T : Any> Producer<T>.observeWhen(whenSign: Producer<Boolean>, listener: (T) -> Unit) =
        observeWhen(whenSign).attach(listener)