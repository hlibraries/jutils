package ru.hnau.jutils.producer

import kotlinx.coroutines.CompletableDeferred
import ru.hnau.jutils.helpers.weak.WeakListener
import ru.hnau.jutils.producer.detacher.ProducerDetacher
import ru.hnau.jutils.producer.detacher.ProducerDetachers
import ru.hnau.jutils.producer.extensions.combine
import ru.hnau.jutils.tryOrElse
import java.util.concurrent.CopyOnWriteArraySet


abstract class Producer<T> {

    private val listeners = CopyOnWriteArraySet<(T) -> Unit>()

    fun attach(listener: (T) -> Unit): ProducerDetacher<T> {
        attachInner(listener)
        return ProducerDetacher(this, listener)
    }

    private fun attachInner(listener: (T) -> Unit) {
        val added = listeners.add(listener)
        if (added) {
            if (listeners.size == 1) {
                onFirstAttachedInner()
            }
            onAttached(listener)
        }
    }

    fun attach(detachers: ProducerDetachers, listener: (T) -> Unit) {
        detachers.addDetacher(ProducerDetacher(this, listener))
        attachInner(listener)
    }

    fun weakAttach(listener: (T) -> Unit) = attach(
            WeakListener(
                    innerListener = listener,
                    onReferenceEmpty = this::detach
            )
    )

    fun weakAttach(detachers: ProducerDetachers, listener: (T) -> Unit) =
            detachers.addDetacher(weakAttach(listener))

    fun detach(listener: (T) -> Unit) {
        val removed = listeners.remove(listener)
        if (removed) {
            onDetached(listener)
            if (listeners.isEmpty()) {
                onLastDetachedInner()
            }
        }
    }

    protected fun call(data: T) {
        listeners.forEach { it.invoke(data) }
    }

    protected open fun onAttached(listener: (T) -> Unit) {}

    private fun onFirstAttachedInner() {
        onFirstAttached()
        onIsObservingChanged(true)
    }

    protected open fun onFirstAttached() {}

    protected open fun onDetached(listener: (T) -> Unit) {}

    private fun onLastDetachedInner() {
        onIsObservingChanged(false)
        onLastDetached()
    }

    protected open fun onLastDetached() {}

    protected open fun onIsObservingChanged(isObserving: Boolean) {}

    /**
     * Additional methods
     */

    companion object

    fun <O : Any> map(converter: (T) -> O) =
            wrap<O> { call(converter.invoke(it)) }

    fun callIf(predicate: (T) -> Boolean) = wrap<Unit> { data ->
        if (predicate.invoke(data)) {
            call(Unit)
        }
    }

    fun <O : Any> mapAsync(converter: (data: T, onConverted: (O) -> Unit) -> Unit) =
            wrap<O> { converter.invoke(it, this::call) }

    fun <O : Any> wrap(onProducedHandler: WrapProducer.Proxy<O>.(T) -> Unit): Producer<O> =
            WrapProducer.create(this, onProducedHandler)

    fun <O : Any> mapNotNull(predicate: (T) -> O?) = wrap<O> { data ->
        val result = predicate.invoke(data) ?: return@wrap
        call(result)
    }

    suspend fun wait(resultFilter: ((T) -> Boolean)? = null): T {

        val detachers = ProducerDetachers()
        val deferred = CompletableDeferred<T>()

        attach(detachers) { value ->
            if (resultFilter?.invoke(value) != false) {
                deferred.complete(value)
            }
        }

        return tryOrElse(
                throwsAction = { deferred.await() },
                onThrow = {
                    deferred.cancel()
                    throw it
                },
                finally = { detachers.detachAllAndClear() }
        )
    }

    fun <P2, R : Any> combineWith(
            other: Producer<P2>,
            combiner: (T, P2) -> R
    ) =
            combine(this, other, combiner)

    fun <P2, P3, R : Any> combineWith(
            other1: Producer<P2>,
            other2: Producer<P3>,
            combiner: (T, P2, P3) -> R
    ) =
            combine(this, other1, other2, combiner)

    fun mapToString() =
            map { it.toString() }

    override fun toString() =
            "Producer{listeners=${listeners.size}}"

}