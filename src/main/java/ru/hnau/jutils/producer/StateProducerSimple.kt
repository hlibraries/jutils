package ru.hnau.jutils.producer

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


class StateProducerSimple<T : Any>(
        initialState: T? = null
) : StateProducer<T>(
        initialState = initialState
), ReadWriteProperty<Any, T?> {

    val currentState: T?
        get() = state

    fun updateState(state: T?) = update(state)
    fun updateStateIfChanged(state: T?) = updateIfChanged(state)
    fun clearState() = clear()

    override operator fun getValue(thisRef: Any, property: KProperty<*>) =
            currentState

    override operator fun setValue(thisRef: Any, property: KProperty<*>, value: T?) =
            updateStateIfChanged(value)

}

fun StateProducerSimple<Boolean>.invert() =
        updateState((currentState ?: false).not())

