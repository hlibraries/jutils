package ru.hnau.jutils.producer.locked_producer


class SimpleLockedProducer(
        isLocked: Boolean = false
) : LockedProducer(
        isLocked
) {

    fun setIsLocked(locked: Boolean) {
        this.locked = locked
    }

}