package ru.hnau.jutils.producer.locked_producer

import ru.hnau.jutils.finisher.Finisher

@Deprecated("Use ru.hnau.jutils.producer.locked_producer.CompletableLockedProducer instead")
class FinishersLockedProducer : LockedProducer(
        isLocked = false
) {

    private var lockedCount = 0
        set(value) {
            field = value
            locked = value > 0
        }

    fun waitFinisher(finisher: Finisher<*>) {
        synchronized(this) { lockedCount++ }

        finisher.await {
            synchronized(this) { lockedCount-- }
        }
    }

}