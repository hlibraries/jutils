package ru.hnau.jutils.producer.locked_producer

import kotlinx.coroutines.CoroutineScope
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.coroutineContext


class SuspendLockedProducer : CalcLockedProducer() {

    operator inline fun <T> invoke(block: () -> T): T {
        return try {
            incLockCount()
            block()
        } finally {
            decLockCount()
        }
    }

    @Deprecated("Should have been protected, but inline fun invoke(block: () -> Unit) expected using only public methods")
    fun incLockCount() =
            incLockedCount()

    @Deprecated("Should have been protected, but inline fun invoke(block: () -> Unit) expected using only public methods")
    fun decLockCount() =
            decLockedCount()

    @Deprecated("Use fun invoke(block: () -> Unit)")
    suspend fun <T> executeLocked(block: suspend CoroutineContext.() -> T): T {
        return try {
            incLockedCount()
            block.invoke(coroutineContext)
        } finally {
            decLockedCount()
        }
    }

}