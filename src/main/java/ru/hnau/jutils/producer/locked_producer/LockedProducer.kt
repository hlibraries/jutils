package ru.hnau.jutils.producer.locked_producer

import ru.hnau.jutils.producer.DataProducer


abstract class LockedProducer(
        isLocked: Boolean = false
) : DataProducer<Boolean>(
        isLocked
) {

    var locked: Boolean
        protected set(value) = updateIfChanged(value)
        get() = data

    override fun toString() =
            "LockedProducer{locked=$locked}"

}