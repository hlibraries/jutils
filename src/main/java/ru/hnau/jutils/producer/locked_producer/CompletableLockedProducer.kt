package ru.hnau.jutils.producer.locked_producer

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Job
import ru.hnau.jutils.finisher.Finisher
import ru.hnau.jutils.helpers.Completable
import ru.hnau.jutils.helpers.asCompletable


class CompletableLockedProducer : CalcLockedProducer() {

    fun wait(completable: Completable<*>) {
        incLockedCount()
        completable.executeOnCompleted { decLockedCount() }
    }

}

fun Job.addToLockedProducer(lockedProducer: CompletableLockedProducer?) =
        apply { lockedProducer?.wait(this.asCompletable) }

fun <T> Deferred<T>.addToLockedProducer(lockedProducer: CompletableLockedProducer?) =
        apply { lockedProducer?.wait(this.asCompletable) }

fun <T : Any> Finisher<T>.addToLockedProducer(lockedProducer: CompletableLockedProducer?) =
        apply { lockedProducer?.wait(this) }