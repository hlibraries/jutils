package ru.hnau.jutils.producer.locked_producer


abstract class CalcLockedProducer : LockedProducer(
        isLocked = false
) {

    private var lockedCount = 0
        set(value) {
            field = value
            locked = value > 0
        }

    private fun updateLockedCount(inc: Boolean) =
            synchronized(this) { lockedCount += if (inc) 1 else -1 }

    protected fun incLockedCount() = updateLockedCount(true)
    protected fun decLockedCount() = updateLockedCount(false)

}