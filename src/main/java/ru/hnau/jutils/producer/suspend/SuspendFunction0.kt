package ru.hnau.jutils.producer.suspend


interface SuspendFunction0<Z> {

    operator suspend fun invoke(): Z

    fun toLambda(): suspend () -> Z = this::invoke

    companion object {

        fun <Z> create(
                getter: suspend () -> Z
        ) = object : SuspendFunction0<Z> {
            override suspend fun invoke() = getter()
        }

    }

}