package ru.hnau.jutils.producer.suspend

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.getter.SuspendGetter
import ru.hnau.jutils.handle
import ru.hnau.jutils.helpers.Outdatable
import ru.hnau.jutils.producer.Producer

abstract class SuspendCacheProducer<T : Any>(
        private val cacheLifetime: TimeValue? = null
) : Producer<SuspendFunction0<T>>(), SuspendFunction0<T> {

    companion object {

        fun <T : Any> create(
                cacheLifetime: TimeValue? = null,
                getter: suspend () -> T
        ) = object : SuspendCacheProducer<T>(cacheLifetime) {
            override suspend fun getNewValue() = getter()
        }

    }

    private var cached: Outdatable<ValueAutoCacheContent<T>>? = null

    protected abstract suspend fun getNewValue(): T

    private val invokeMutex = Mutex()

    override suspend fun invoke() = invokeMutex.withLock<T> {
        val cachedValue = cached?.value.handle(
                ifNotNull = { it },
                ifNull = {
                    ValueAutoCacheContent
                            .tryOrError<T> { getNewValue() }
                            .apply(this::update)
                }
        )
        return@withLock cachedValue.check(
                ifValue = { it },
                ifError = { throw it }
        )
    }

    fun updateOutdatable(value: Outdatable<ValueAutoCacheContent<T>>?) {
        cached = value
    }

    fun update(value: ValueAutoCacheContent<T>?) =
            updateOutdatable(value?.let { Outdatable(it, cacheLifetime) })

    fun updateError(error: Throwable) =
            update(ValueAutoCacheContent.fromError<T>(error))

    fun updateData(value: T) =
            update(ValueAutoCacheContent.fromValue(value))

    fun invalidate() {
        updateOutdatable(null)
        call(this)
    }

    override fun onAttached(
            listener: (SuspendFunction0<T>) -> Unit
    ) {
        super.onAttached(listener)

        synchronized(this) {

            val needInvalidate = cached?.value.handle(
                    ifNull = { false },
                    ifNotNull = { cachedValue -> cachedValue.check(forValue = false, forError = true) }
            )

            needInvalidate.handle(
                    onTrue = this::invalidate,
                    onFalse = { listener(this) }

            )

        }
    }

}