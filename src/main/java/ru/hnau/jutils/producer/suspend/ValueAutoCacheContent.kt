package ru.hnau.jutils.producer.suspend

import ru.hnau.jutils.handle
import ru.hnau.jutils.tryOrElse


class ValueAutoCacheContent<T> private constructor(
        val value: T?,
        val error: Throwable?
) {

    companion object {

        fun <T> fromValue(value: T) =
                ValueAutoCacheContent(value = value, error = null)

        fun <T> fromError(error: Throwable) =
                ValueAutoCacheContent<T>(value = null, error = error)

        inline fun <T> tryOrError(
                block: () -> T
        ) = tryOrElse(
                throwsAction = { fromValue(block()) },
                onThrow = this::fromError
        )

    }

    inline fun <R> check(
            ifValue: (T) -> R,
            ifError: (Throwable) -> R
    ) = error.handle(
            ifNotNull = ifError,
            ifNull = { ifValue(value!!) }
    )

    inline fun <R> check(
            forValue: R,
            ifError: (Throwable) -> R
    ) = check(
            ifValue = { forValue },
            ifError = ifError
    )

    inline fun <R> check(
            ifValue: (T) -> R,
            forError: R
    ) = check(
            ifValue = ifValue,
            ifError = { forError }
    )

    fun <R> check(
            forValue: R,
            forError: R
    ) = check(
            forValue = forValue,
            ifError = { forError }
    )

}