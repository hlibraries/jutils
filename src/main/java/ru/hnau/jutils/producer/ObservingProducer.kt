package ru.hnau.jutils.producer

import ru.hnau.jutils.producer.detacher.ProducerDetachers


class ObservingProducer<T : Any>(
        private val producersProducer: Producer<Producer<T>>
) : StateProducer<T>() {

    private val detachers = ProducerDetachers()

    private var producerInner: Producer<T>? = null
        set(value) = synchronized(this) {
            field?.detach(this::update)
            field = value
            value?.attach(this::update)
        }

    override fun onFirstAttached() {
        super.onFirstAttached()
        producersProducer.attach(detachers) { producerInner = it }
    }

    override fun onLastDetached() {
        detachers.detachAllAndClear()
        super.onLastDetached()
    }

}