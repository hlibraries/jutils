package ru.hnau.jutils.producer

import java.util.concurrent.atomic.AtomicInteger


open class ActualProducer<T>(
        initialState: T
) : Producer<T>() {

    protected var state: T = initialState
        private set

    protected fun update(newState: T) {
        synchronized(this) {
            state = newState
            call(newState)
        }
    }

    protected fun updateIfChanged(newState: T) =
            synchronized(this) {
                if (newState != state) {
                    update(newState)
                }
            }

    override fun onAttached(listener: (T) -> Unit) {
        super.onAttached(listener)
        listener.invoke(state)
    }

}