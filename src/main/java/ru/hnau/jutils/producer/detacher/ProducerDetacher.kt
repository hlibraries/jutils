package ru.hnau.jutils.producer.detacher

import ru.hnau.jutils.producer.Producer


class ProducerDetacher<T>(
        private val producer: Producer<T>,
        private val listener: (T) -> Unit
) {

    fun detach() = producer.detach(listener)

}