package ru.hnau.jutils.producer

import java.util.concurrent.atomic.AtomicInteger


open class StateProducer<T : Any>(
        initialState: T? = null
) : Producer<T>() {

    private val attachedCount = AtomicInteger()

    protected var state: T? = initialState
        private set

    protected fun clear() =
            update(null)

    protected fun update(newState: T?) {
        synchronized(this) {
            state = newState
            state?.let(this::call)
        }
    }

    protected fun updateIfChanged(newState: T?) =
            synchronized(this) {
                if (newState != state) {
                    update(newState)
                }
            }

    override fun onAttached(listener: (T) -> Unit) {
        super.onAttached(listener)
        state?.let(listener)
    }

}