package ru.hnau.jutils.producer

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import ru.hnau.jutils.coroutines.launch
import ru.hnau.jutils.getter.base.GetterAsync
import ru.hnau.jutils.getter.base.get
import ru.hnau.jutils.ifTrue
import ru.hnau.jutils.tryOrElse
import kotlin.coroutines.CoroutineContext


abstract class AsyncWrapProducer<I : Any, R : Any>(
        source: Producer<GetterAsync<Unit, I>>
) : WrapProducer<GetterAsync<Unit, I>, R>(
        source = source
) {

    companion object {

        fun <I : Any, R : Any> create(
                source: Producer<GetterAsync<Unit, I>>,
                onStartResult: R?,
                getOnSuccessResult: (I) -> R?,
                getOnErrorResult: (Throwable) -> R?,
                coroutinesExecutor: (suspend CoroutineScope.() -> Unit) -> Unit
        ) = object : AsyncWrapProducer<I, R>(
                source = source
        ) {

            override fun onStartGetting() =
                    callIfExists(onStartResult)

            override fun onGettingSuccess(data: I) =
                    callIfExists(getOnSuccessResult(data))

            override fun onGettingError(error: Throwable) =
                    callIfExists(getOnErrorResult(error))

            override fun executeCoroutine(coroutine: suspend CoroutineScope.() -> Unit) =
                    coroutinesExecutor(coroutine)

            private fun callIfExists(value: R?) {
                value?.let(this::call)
            }

        }

    }

    private var currentGetter: GetterAsync<Unit, I>? = null

    protected abstract fun onStartGetting()
    protected abstract fun onGettingSuccess(data: I)
    protected abstract fun onGettingError(error: Throwable)

    protected abstract fun executeCoroutine(coroutine: suspend CoroutineScope.() -> Unit)

    override fun onWrappedProducerCall(data: GetterAsync<Unit, I>, proxy: Proxy<R>) {
        synchronized(this@AsyncWrapProducer) {
            if (currentGetter == null) {
                onStartGetting()
            }
            currentGetter = data
        }
        executeCoroutine {
            tryOrElse(
                    throwsAction = {
                        val result = data.get()
                        finishWithActualGetter(data) {
                            onGettingSuccess(result)
                        }
                    },
                    onThrow = { error ->
                        finishWithActualGetter(data) {
                            onGettingError(error)
                        }
                    }
            )
        }
    }

    private inline fun finishWithActualGetter(
            getter: GetterAsync<Unit, I>,
            action: () -> Unit
    ) = synchronized(this) {
        if (getter == currentGetter) {
            action()
            currentGetter = null
        }
    }


}

fun <I : Any, R : Any> Producer<GetterAsync<Unit, I>>.toSync(
        onStartResult: R?,
        getOnSuccessResult: (I) -> R?,
        getOnErrorResult: (Throwable) -> R?,
        coroutinesExecutor: (suspend CoroutineScope.() -> Unit) -> Unit
): Producer<R> = AsyncWrapProducer.create(
        source = this,
        onStartResult = onStartResult,
        getOnSuccessResult = getOnSuccessResult,
        getOnErrorResult = getOnErrorResult,
        coroutinesExecutor = coroutinesExecutor
)
