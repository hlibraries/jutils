package ru.hnau.jutils.producer


abstract class AlwaysProducer<T> : Producer<T>() {

    companion object {

        fun <T> create(getter: () -> T): Producer<T> =
                object : AlwaysProducer<T>() {
                    override val value: T
                        get() = getter.invoke()
                }

        fun <T> create(value: T): Producer<T> =
                object : AlwaysProducer<T>() {
                    override val value = value
                }

    }

    protected abstract val value: T

    protected fun onValueChanged() =
            call(value)

    override fun onAttached(listener: (T) -> Unit) {
        super.onAttached(listener)
        listener.invoke(value)
    }

}