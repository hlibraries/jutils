package ru.hnau.jutils.producer

import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.handle
import ru.hnau.jutils.helpers.*


abstract class CachingProducer<T : Any>(
        private val valueLifetime: TimeValue? = null
) : Producer<T>() {

    companion object {

        fun <T : Any> create(
                valueLifetime: TimeValue? = null,
                valueGetter: () -> T
        ) = object : CachingProducer<T>(valueLifetime) {
            override fun getNewValue() =
                    valueGetter.invoke()
        }

    }

    private var isObserving = false

    private var cachedValue: Outdatable<T>? = null

    protected val existence: T?
        get() = cachedValue?.value

    protected abstract fun getNewValue(): T

    fun update(newValue: T?) {
        synchronized(this) {
            cachedValue = newValue?.toOutdatable(valueLifetime)
            newValue?.let(this::call)
        }
    }

    fun updateIfChanged(newValue: T?) =
            synchronized(this) {
                if (newValue != existence) {
                    update(newValue)
                }
            }

    fun clear() =
            update(null)

    fun invalidate() = synchronized(this) {
        if (isObserving) forceInvalidate() else clear()
    }

    fun forceInvalidate() = synchronized(this) {
        update(getNewValue())
    }

    override fun onAttached(listener: (T) -> Unit) {
        super.onAttached(listener)
        synchronized(this) {
            cachedValue?.value?.let { cachedValue ->
                listener.invoke(cachedValue)
                return@synchronized
            }
            invalidate()
        }
    }

    override fun onIsObservingChanged(isObserving: Boolean) {
        super.onIsObservingChanged(isObserving)
        this.isObserving = isObserving
    }

}