package ru.hnau.jutils.producer


@Deprecated("Use AlwaysProducer")
class SimpleDataProducer<T>(
        initialData: T
) : DataProducer<T>(
        initialData
) {

    var content: T
        set(value) {
            data = value
        }
        get() = data

}