package ru.hnau.jutils.producer

import ru.hnau.jutils.producer.detacher.ProducerDetachers


abstract class WrapProducer<I, O : Any>(
        private val source: Producer<I>
) : StateProducer<O>() {

    interface Proxy<T> {
        fun call(value: T)
    }

    companion object {

        fun <I, O : Any> create(
                source: Producer<I>,
                onWrappedProducerCall: Proxy<O>.(I) -> Unit
        ): Producer<O> = object : WrapProducer<I, O>(source) {
            override fun onWrappedProducerCall(data: I, proxy: Proxy<O>) =
                    onWrappedProducerCall.invoke(proxy, data)
        }

    }

    private val detachers = ProducerDetachers()

    private val proxy = object : Proxy<O> {
        override fun call(value: O) = update(value)
    }

    abstract fun onWrappedProducerCall(data: I, proxy: Proxy<O>)

    override fun onFirstAttached() {
        super.onFirstAttached()
        source.attach(detachers) { value ->
            onWrappedProducerCall(value, proxy)
        }
    }

    override fun onLastDetached() {
        detachers.detachAllAndClear()
        super.onLastDetached()
    }

}