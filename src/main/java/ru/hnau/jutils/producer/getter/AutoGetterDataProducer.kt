package ru.hnau.jutils.producer.getter

import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.getter.MutableGetter

@Deprecated("Use ru.hnau.jutils.producer.container.DeferredPossibleContainer instead")
open class AutoGetterDataProducer<T : Any>(
        override val getter: MutableGetter<Unit, T>
) : GetterDataProducer<T>() {

    constructor(
            valueLifetime: TimeValue? = null,
            getter: () -> T
    ) : this(MutableGetter.simple(valueLifetime, getter))

}