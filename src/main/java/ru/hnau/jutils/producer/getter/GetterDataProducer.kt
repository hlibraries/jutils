package ru.hnau.jutils.producer.getter

import ru.hnau.jutils.getter.MutableGetter
import ru.hnau.jutils.getter.base.get
import ru.hnau.jutils.producer.Producer


@Deprecated("Use ru.hnau.jutils.producer.container.DeferredPossibleContainer instead")
abstract class GetterDataProducer<T : Any> : Producer<T>() {

    protected abstract val getter: MutableGetter<Unit, T>

    private var isObserving = false

    val currentData: T
        get() = getter.get()

    override fun onAttached(listener: (T) -> Unit) {
        super.onAttached(listener)

        synchronized(this) {
            val existenceValue = getter.existence
            if (existenceValue != null) {
                listener.invoke(existenceValue)
            } else {
                invalidate()
            }
        }

    }

    fun invalidate() = synchronized(this) {
        getter.clear()
        if (isObserving) {
            call(getter.get())
        }
    }

    override fun onIsObservingChanged(isObserving: Boolean) {
        super.onIsObservingChanged(isObserving)
        this.isObserving = isObserving
    }

}