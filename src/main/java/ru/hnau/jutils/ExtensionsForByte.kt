package ru.hnau.jutils


infix fun Byte.shr(bitCount: Int) =
        (toInt() shr bitCount).toByte()

infix fun Byte.shl(bitCount: Int) =
        (toInt() shl bitCount).toByte()

infix fun Byte.and(other: Byte) =
        (toInt() and other.toInt()).toByte()

infix fun Byte.or(other: Byte) =
        (toInt() or other.toInt()).toByte()

infix fun Byte.xor(other: Byte) =
        (toInt() xor other.toInt()).toByte()