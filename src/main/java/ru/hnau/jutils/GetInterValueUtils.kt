package ru.hnau.jutils


fun getIntInterInts(fromValue: Int, toValue: Int, position: Float) =
        fromValue + ((toValue - fromValue) * position).toInt()

fun getIntInterInts(fromValue: Int, toValue: Int, position: Double) =
        getIntInterInts(fromValue, toValue, position.toFloat())


fun getByteInterBytes(fromValue: Byte, toValue: Byte, position: Float) =
        fromValue + ((toValue - fromValue) * position).toByte()

fun getByteInterBytes(fromValue: Byte, toValue: Byte, position: Double) =
        getByteInterBytes(fromValue, toValue, position.toFloat())


fun getLongInterLongs(fromValue: Long, toValue: Long, position: Float) =
        fromValue + ((toValue - fromValue) * position).toLong()

fun getLongInterLongs(fromValue: Long, toValue: Long, position: Double) =
        getLongInterLongs(fromValue, toValue, position.toFloat())


fun getShortInterShorts(fromValue: Short, toValue: Short, position: Float) =
        fromValue + ((toValue - fromValue) * position).toShort()

fun getShortInterShorts(fromValue: Short, toValue: Short, position: Double) =
        getShortInterShorts(fromValue, toValue, position.toFloat())


fun getFloatInterFloats(fromValue: Float, toValue: Float, position: Float) =
        fromValue + (toValue - fromValue) * position

fun getFloatInterFloats(fromValue: Float, toValue: Float, position: Double) =
        getFloatInterFloats(fromValue, toValue, position.toFloat())


fun getDoubleInterDoubles(fromValue: Double, toValue: Double, position: Float) =
        fromValue + (toValue - fromValue) * position

fun getDoubleInterDoubles(fromValue: Double, toValue: Double, position: Double) =
        getDoubleInterDoubles(fromValue, toValue, position.toFloat())
