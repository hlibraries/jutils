package ru.hnau.jutils

import kotlinx.coroutines.*
import ru.hnau.jutils.coroutines.JobExecutor
import ru.hnau.jutils.coroutines.TasksFinalizer
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.coroutines.delay
import ru.hnau.jutils.handle
import ru.hnau.jutils.producer.AlwaysProducer
import java.io.File


object Main {

    private const val FILE_PATH = "/home/hnau/programs/programming/AndroidStudio/SDK/sources/android-28/android/view/View.java"


    @JvmStatic
    fun main(args: Array<String>) = runBlocking {

        val file = File(FILE_PATH)
        file.forEachLine {line ->
            line.startsWith("    public void set").ifFalse { return@forEachLine }
            val nameWithParams = line.drop(19).dropLast(3).split("(")
            val name = nameWithParams[0]
            val params = nameWithParams[1]
            println("$name - $params")
        }

    }

}