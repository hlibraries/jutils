package ru.hnau.jutils

import java.util.*


inline class TimeValue(
        val milliseconds: Long
) : Comparable<TimeValue> {

    constructor(calendar: Calendar) : this(calendar.timeInMillis)

    constructor(date: Date) : this(date.time)

    companion object {

        fun now() =
                TimeValue(System.currentTimeMillis())

        fun fromNanoseconds(nanoseconds: Long) =
                fromMicroseconds(nanoseconds / 1000L)

        fun fromMicroseconds(microseconds: Long) =
                TimeValue(microseconds / 1000L)

        fun isAfterPause(previousTime: TimeValue, pause: TimeValue) =
                now() > previousTime + pause

        fun isInPause(previousTime: TimeValue, pause: TimeValue) =
                now() < previousTime + pause

        val ZERO = TimeValue(0)
        val MILLISECOND = TimeValue(1)
        val SECOND = MILLISECOND * 1000
        val MINUTE = SECOND * 60
        val HOUR = MINUTE * 60
        val DAY = HOUR * 24
        val WEEK = DAY * 7

    }

    val seconds: Long
        get() = milliseconds / 1000

    val minutes: Long
        get() = seconds / 60

    val hours: Long
        get() = minutes / 60

    val days: Long
        get() = hours / 24


    val millisecondsDigits: Int
        get() = (milliseconds - seconds * 1000).toInt()

    val secondsDigits: Int
        get() = (seconds - minutes * 60).toInt()

    val minutesDigits: Int
        get() = (minutes - hours * 60).toInt()

    val hoursDigits: Int
        get() = (hours - days * 24).toInt()

    val daysDigits: Int
        get() = days.toInt()


    fun toCalendar() = Calendar.getInstance().apply { timeInMillis = milliseconds }!!

    fun toDate() = Date(milliseconds)

    fun toMillisecondsString() = "${milliseconds}ms"
    fun toSecondsString() = "${seconds}s"
    fun toMinutesString() = "${minutes}m"
    fun toHoursString() = "${hours}h"
    fun toDaysString() = "${days}d"

    override fun toString() = "TimeValue(milliseconds=$milliseconds)"

    fun toLevelsString(levels: Int = 3) =
            listOf(
                    daysDigits to "d",
                    hoursDigits to "h",
                    minutesDigits to "m",
                    secondsDigits to "s",
                    millisecondsDigits to "ms"
            )
                    .dropWhile { it.first <= 0 }
                    .take(levels)
                    .takeIfNotEmpty()
                    ?.joinToString(
                            separator = "",
                            transform = { it.first.toString() + it.second }
                    ) ?: "0ms"

    fun isActual(lifetime: TimeValue?) =
            lifetime == null || this + lifetime >= now()

    fun sleep() = Thread.sleep(milliseconds)

    operator fun plus(other: TimeValue) = TimeValue(this.milliseconds + other.milliseconds)
    operator fun minus(other: TimeValue) = TimeValue(this.milliseconds - other.milliseconds)
    operator fun times(count: Byte) = TimeValue(milliseconds * count)
    operator fun times(count: Int) = TimeValue(milliseconds * count)
    operator fun times(count: Long) = TimeValue(milliseconds * count)
    operator fun times(count: Float) = TimeValue((milliseconds * count).toLong())
    operator fun times(count: Double) = TimeValue((milliseconds * count).toLong())
    operator fun div(count: Byte) = TimeValue(milliseconds / count)
    operator fun div(count: Int) = TimeValue(milliseconds / count)
    operator fun div(count: Long) = TimeValue(milliseconds / count)
    operator fun div(count: Float) = TimeValue((milliseconds / count).toLong())
    operator fun div(count: Double) = TimeValue((milliseconds / count).toLong())
    operator fun div(other: TimeValue) = milliseconds.toDouble() / other.milliseconds.toDouble()

    override operator fun compareTo(other: TimeValue) = this.milliseconds.compareTo(other.milliseconds)

}

fun sleep(time: TimeValue) =
        time.sleep()