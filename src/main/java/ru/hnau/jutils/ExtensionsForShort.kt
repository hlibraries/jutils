package ru.hnau.jutils


infix fun Short.shr(bitCount: Int) =
        (toInt() shr bitCount).toShort()

infix fun Short.shl(bitCount: Int) =
        (toInt() shl bitCount).toShort()

infix fun Short.and(other: Short) =
        (toInt() and other.toInt()).toShort()

infix fun Short.or(other: Short) =
        (toInt() or other.toInt()).toShort()

infix fun Short.xor(other: Short) =
        (toInt() xor other.toInt()).toShort()