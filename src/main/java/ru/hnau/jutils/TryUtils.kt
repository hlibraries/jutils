package ru.hnau.jutils


inline fun <R> tryOrElse(
        throwsAction: () -> R,
        onThrow: (th: Throwable) -> R,
        finally: () -> Unit = {}
) =
        try {
            throwsAction.invoke()
        } catch (th: Throwable) {
            onThrow.invoke(th)
        } finally {
            finally.invoke()
        }

inline fun <R> tryCatch(
        throwsAction: () -> R,
        onThrow: (th: Throwable) -> Unit,
        finally: () -> Unit = {}
) = tryOrElse(
        throwsAction = throwsAction,
        onThrow = { onThrow.invoke(it); null },
        finally = finally
)

inline fun <R> tryOrNull(
        throwsAction: () -> R
) = tryOrElse(
        throwsAction = throwsAction,
        onThrow = { null }
)

inline fun tryCatch(
        throwsAction: () -> Unit
) = tryOrElse(
        throwsAction = { throwsAction.invoke(); true },
        onThrow = { false }
)

val Throwable.asException: Exception
    get() = this as? Exception ?: Exception(this)

val Throwable?.asExceptionOrNull: Exception?
    get() = this?.asException