package ru.hnau.jutils

fun Boolean?.isTrueOrNull() = this != false

fun Boolean?.isFalseOrNull() = this != true

inline fun <R> Boolean.handle(onTrue: () -> R, onFalse: () -> R) =
        if (this) onTrue.invoke() else onFalse.invoke()

inline fun <R> Boolean?.handle(onTrue: () -> R, onFalse: () -> R, onNull: () -> R) = when (this) {
    true -> onTrue.invoke()
    false -> onFalse.invoke()
    null -> onNull.invoke()
}

fun <R> Boolean.handle(forTrue: R, forFalse: R) =
        if (this) forTrue else forFalse

fun <R> Boolean?.handle(forTrue: R, forFalse: R, forNull: R) = when (this) {
    true -> forTrue
    false -> forFalse
    null -> forNull
}

inline fun <R : Any> Boolean?.ifTrue(action: () -> R) =
        if (this == true) action.invoke() else null

fun <R : Any> Boolean?.ifTrue(value: R) =
        if (this == true) value else null

inline fun <R : Any> Boolean?.ifFalse(action: () -> R) =
        if (this == false) action.invoke() else null

fun <R : Any> Boolean?.ifFalse(value: R) =
        if (this == false) value else null

inline fun <R : Any> Boolean?.ifTrueOrNull(action: () -> R) =
        if (this == null || this) action.invoke() else null

fun <R : Any> Boolean?.ifTrueOrNull(value: R) =
        if (this == null || this) value else null

inline fun <R : Any> Boolean?.ifFalseOrNull(action: () -> R) =
        if (this == null || !this) action.invoke() else null

fun <R : Any> Boolean?.ifFalseOrNull(value: R) =
        if (this == null || !this) value else null

fun Boolean.toLong() = if (this) 1L else 0L
fun Boolean.toInt() = if (this) 1 else 0
fun Boolean.toByte() = toInt().toByte()
fun Boolean.toShort() = toInt().toShort()
fun Boolean.toFloat() = toInt().toFloat()
fun Boolean.toDouble() = toInt().toDouble()