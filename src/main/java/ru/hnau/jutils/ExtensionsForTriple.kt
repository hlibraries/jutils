package ru.hnau.jutils


inline fun <I, O> Triple<I, I, I>.map(converter: (I) -> O) =
        Triple(converter.invoke(first), converter.invoke(second), converter.invoke(third))

inline fun <T> Triple<T, T, T>.forEach(action: (T) -> Unit) {
    action.invoke(first)
    action.invoke(second)
    action.invoke(third)
}