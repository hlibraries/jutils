package ru.hnau.jutils.finisher

import ru.hnau.jutils.possible.*
import java.util.*


fun <T : Any> List<Finisher<Possible<T>>>.onlySuccess(): Finisher<List<T>> =
        Finisher { onFinished ->

            if (isEmpty()) {
                onFinished.invoke(emptyList())
                return@Finisher
            }

            var notFinishedCount = size
            val result = LinkedList<T>()

            val decAndReturnIfNeed = {
                notFinishedCount--
                if (notFinishedCount <= 0) {
                    onFinished.invoke(result)
                }
            }

            forEach {
                it.await(
                        onSuccess = { finisherResult ->
                            synchronized(result) {
                                result.add(finisherResult)
                                decAndReturnIfNeed.invoke()
                            }
                        },
                        onError = {
                            synchronized(result) {
                                decAndReturnIfNeed.invoke()
                            }
                        }
                )
            }
        }

fun <T : Any> List<Finisher<Possible<T>>>.allSuccess(): Finisher<Possible<List<T>>> =
        Finisher { onFinished ->

            if (isEmpty()) {
                onFinished.success(emptyList())
                return@Finisher
            }

            val result = LinkedList<T>()

            forEach {
                it.await(
                        onSuccess = { finisherResult ->
                            synchronized(result) {
                                result.add(finisherResult)
                                if (result.size >= size) {
                                    onFinished.success(result)
                                }
                            }
                        },
                        onError = onFinished::errorOrUndefined
                )
            }
        }

fun <T : Any> List<Finisher<Possible<List<T>>>>.flattenIfAllSuccess(): Finisher<Possible<List<T>>> =
        allSuccess().map { possibleListOfLists -> possibleListOfLists.map { it.flatten() } }

fun <T : Any> List<Finisher<Possible<List<T>>>>.flattenOfSuccess(): Finisher<List<T>> =
        all().map { data -> data.mapNotNull { it.data }.flatten() }

fun <T : Any> List<Finisher<Possible<T>>>.firstSuccess(): Finisher<Possible<T>> =
        Finisher { onFinished ->

            if (isEmpty()) {
                onFinished.undefined()
                return@Finisher
            }

            val locker = Object()
            var firstError: Exception? = null
            var errorsCount = 0

            forEach {
                it.await(
                        onSuccess = onFinished::success,
                        onError = {ex ->
                            synchronized(locker) {
                                firstError = firstError ?: ex
                                errorsCount++
                                if (errorsCount >= size) {
                                    onFinished.error(firstError)
                                }
                            }
                        }
                )
            }
        }