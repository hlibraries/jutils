package ru.hnau.jutils.finisher

import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.possible.success
import ru.hnau.jutils.possible.undefined
import java.util.*

fun <T> List<Finisher<List<T>>>.flatten() =
        all().map(List<List<T>>::flatten)

fun <T> List<Finisher<T>>.all(): Finisher<List<T>> =
        Finisher { onFinished ->

            if (isEmpty()) {
                onFinished.invoke(emptyList())
                return@Finisher
            }

            val result = LinkedList<T>()

            forEach {
                it.await {finisherResult ->
                    synchronized(result) {
                        result.add(finisherResult)
                        if (result.size == size) {
                            onFinished.invoke(result)
                        }
                    }
                }
            }
        }

fun <T : Any> List<Finisher<T>>.first(): Finisher<Possible<T>> =
        Finisher { onFinished ->
            if (isEmpty()) {
                onFinished.undefined()
                return@Finisher
            }
            forEach { it.await { finisherResult -> onFinished.success(finisherResult) } }
        }