package ru.hnau.jutils.finisher

import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.possible.errorOrUndefined
import ru.hnau.jutils.possible.success
import ru.hnau.jutils.producer.Producer


fun <T : Any> Finisher.Companion.success(data: T) =
        forExistenceData(Possible.success(data))

fun <T : Any> Finisher.Companion.successOrUndefined(data: T?) =
        forExistenceData(Possible.successOrUndefined(data))

fun <T : Any> Finisher.Companion.successOrError(data: T?) =
        forExistenceData(Possible.successOrError(data))

fun <T : Any> Finisher.Companion.error(error: Exception? = null) =
        forExistenceData(Possible.error<T>(error))

fun <T : Any> Finisher.Companion.error(message: String?) =
        forExistenceData(Possible.error<T>(message))

fun <T : Any> Finisher.Companion.errorOrUndefined(error: Exception?) =
        forExistenceData(Possible.errorOrUndefined<T>(error))

fun <T : Any> Finisher.Companion.error(th: Throwable?) =
        forExistenceData(Possible.error<T>(th))

fun <T : Any> Finisher.Companion.errorOrUndefined(th: Throwable?) =
        forExistenceData(Possible.errorOrUndefined<T>(th))

fun <T : Any> Finisher.Companion.errorOrUndefined(message: String?) =
        forExistenceData(Possible.errorOrUndefined<T>(message))

fun <T : Any> Finisher.Companion.undefined() =
        forExistenceData(Possible.undefined<T>())

inline fun <T : Any> Finisher.Companion.trySuccessCatchError(throwsAction: () -> T) =
        forExistenceData(Possible.trySuccessCatchError(throwsAction))

inline fun <T : Any> Finisher.Companion.trySuccessCatchUndefined(throwsAction: () -> T) =
        forExistenceData(Possible.trySuccessCatchUndefined(throwsAction))

inline fun <T : Any> Finisher.Companion.trySuccessCatchErrorPossible(throwsAction: () -> Possible<T>) =
        forExistenceData(Possible.trySuccessCatchErrorPossible(throwsAction))

inline fun <T : Any> Finisher.Companion.trySuccessCatchUndefinedPossible(throwsAction: () -> Possible<T>) =
        forExistenceData(Possible.trySuccessCatchUndefinedPossible(throwsAction))

fun Finisher.Companion.success() = Finisher.success(Unit)

fun <T : Any> Finisher<Possible<T>>.await(
        onSuccess: (T) -> Unit,
        onError: (Exception) -> Unit,
        onUndefined: () -> Unit = {}
) =
        await {
            it.handle(
                    onSuccess,
                    onError,
                    onUndefined
            )
        }


fun <T : Any> Finisher<Possible<T>>.await(
        onSuccess: (T) -> Unit,
        onError: (Exception?) -> Unit = {}
) =
        await {
            it.handle(
                    onSuccess,
                    onError
            )
        }

fun <T : Any> Finisher<Possible<T>>.awaitSuccess(
        onSuccess: (T) -> Unit
) = await { it.ifSuccess(onSuccess) }

fun <T : Any> Finisher<Possible<T>>.awaitError(
        onError: (Exception) -> Unit
) = await { it.ifError(onError) }

fun <T : Any> Finisher<Possible<T>>.awaitUndefined(
        onUndefined: () -> Unit
) = await { it.ifUndefined(onUndefined) }

fun <T : Any> Finisher<Possible<T>>.awaitSuccessOrUndefined(
        onSuccessOrUndefined: (T?) -> Unit
) = await { it.ifSuccessOrUndefined(onSuccessOrUndefined) }

fun <T : Any> Finisher<Possible<T>>.awaitSuccessOrError(
        onSuccessOrError: (T?) -> Unit
) = await { it.ifSuccessOrError(onSuccessOrError) }

fun <T : Any> Finisher<Possible<T>>.awaitErrorOrUndefined(
        onErrorOrUndefined: (Exception?) -> Unit
) = await { it.ifErrorOrUndefined(onErrorOrUndefined) }

fun <T : Any> Finisher<Possible<T>>.applyAwaitSuccess(
        onSuccess: (T) -> Unit
) = apply { awaitSuccess(onSuccess) }

fun <T : Any> Finisher<Possible<T>>.applyAwaitError(
        onError: (Exception) -> Unit
) = apply { awaitError(onError) }

fun <T : Any> Finisher<Possible<T>>.applyAwaitUndefined(
        onUndefined: () -> Unit
) = apply { awaitUndefined(onUndefined) }

fun <T : Any> Finisher<Possible<T>>.applyAwaitSuccessOrUndefined(
        onSuccessOrUndefined: (T?) -> Unit
) = apply { awaitSuccessOrUndefined(onSuccessOrUndefined) }

fun <T : Any> Finisher<Possible<T>>.applyAwaitSuccessOrError(
        onSuccessOrError: (T?) -> Unit
) = apply { awaitSuccessOrError(onSuccessOrError) }

fun <T : Any> Finisher<Possible<T>>.applyAwaitErrorOrUndefined(
        onErrorOrUndefined: (Exception?) -> Unit
) = apply { awaitErrorOrUndefined(onErrorOrUndefined) }

fun <T : Any> Finisher<Possible<T>>.applyAwait(
        onSuccess: (T) -> Unit,
        onError: (Exception) -> Unit,
        onUndefined: () -> Unit
) = applyAwait { it.handle(onSuccess, onError, onUndefined) }

fun <T : Any> Finisher<Possible<T>>.applyAwait(
        onSuccess: (T) -> Unit,
        onError: (Exception?) -> Unit
) = applyAwait { it.handle(onSuccess, onError) }

operator fun <T : Any, O : Any> Finisher<Possible<T>>.times(other: Finisher<Possible<O>>): Finisher<Possible<Pair<T, O>>> =
        Finisher { onFinished ->
            this.await(
                    onSuccess = { firstValue ->
                        other.await(
                                onSuccess = { secondValue ->
                                    onFinished.success(firstValue to secondValue)
                                },
                                onError = { onFinished.errorOrUndefined(it) }
                        )
                    },
                    onError = { onFinished.errorOrUndefined(it) }
            )
        }

fun <T : Any> Finisher<Possible<T>>.fitInTimeOrError(timeOut: TimeValue) =
        fitInTimeOrElse(timeOut) { Possible.error(it) }


fun <T : Any> Finisher.Companion.fromProducerOrErrorIfTimeout(
        producer: Producer<Possible<T>>,
        timeOut: TimeValue,
        resultFilter: ((T) -> Boolean)? = null
) = Finisher.fromProducerOrElseIfTimeout(
        producer = producer,
        converter = { it },
        timeoutAction = { Possible.error(it) },
        resultFilter = { it.data?.let { data -> resultFilter?.invoke(data) ?: true } ?: false },
        timeOut = timeOut
)


fun <T : Any> Finisher<T>.fitInTimeOrPossibleError(timeOut: TimeValue) =
        fitInTimeOrElse(
                timeOut = timeOut,
                fitInTimeAction = { Possible.success(it) },
                elseAction = { Possible.error(it) }
        )