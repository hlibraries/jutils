package ru.hnau.jutils.finisher


@Deprecated("Use kotlinx.coroutines.Deferred instead")
class AutoFinisher<T> private constructor(
        private val finish: (T) -> Unit,
        callback: (finish: (T) -> Unit) -> Unit
) : Finisher<T>(callback) {

    companion object {

        fun <T> create(): AutoFinisher<T> {
            var finishFinisher: (T) -> Unit = {}
            val finish: (T) -> Unit = { finishFinisher.invoke(it) }
            return AutoFinisher(
                    finish,
                    { finishFinisher = it }
            )
        }

    }

    fun finish(result: T) = finish.invoke(result)

}