package ru.hnau.jutils.finisher.detacher


interface FinisherDetacher {

    fun detach()

}