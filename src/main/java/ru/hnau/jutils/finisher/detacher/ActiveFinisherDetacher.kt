package ru.hnau.jutils.finisher.detacher

import ru.hnau.jutils.finisher.Finisher


class ActiveFinisherDetacher<T>(
        private val finisher: Finisher<T>,
        private val listener: (T) -> Unit
) : FinisherDetacher {

    override fun detach() {
        finisher.detach(listener)
    }

}