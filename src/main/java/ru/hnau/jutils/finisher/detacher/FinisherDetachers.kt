package ru.hnau.jutils.finisher.detacher

import java.util.*

class FinisherDetachers {

    private val finishers = LinkedList<FinisherDetacher>()

    fun addDetacher(detacher: FinisherDetacher) {
        synchronized(finishers) {
            finishers.add(detacher)
        }
    }

    fun detachAllAndClear() = synchronized(this) {
        finishers.forEach(FinisherDetacher::detach)
        finishers.clear()
    }

}