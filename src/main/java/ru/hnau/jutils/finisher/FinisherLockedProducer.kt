package ru.hnau.jutils.finisher

import ru.hnau.jutils.producer.locked_producer.LockedProducer


class FinisherLockedProducer : LockedProducer(true) {

    fun onFinished() {
        locked = false
    }

}