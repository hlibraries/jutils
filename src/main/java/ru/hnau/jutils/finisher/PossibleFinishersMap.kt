package ru.hnau.jutils.finisher

import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.possible.error
import ru.hnau.jutils.possible.success
import ru.hnau.jutils.possible.undefined
import ru.hnau.jutils.tryOrElse


fun <T : Any, O : Any> Finisher<Possible<T>>.mapPossible(
        converter: (T) -> O
) =
        Finisher<Possible<O>> { onFinished ->
            await { data ->
                val convertedData = data.map(converter)
                onFinished.invoke(convertedData)
            }
        }

fun <T : Any, O : Any> Finisher<Possible<T>>.mapPossible(data: O) =
        mapPossible { _ -> data }

fun <T : Any, O : Any> Finisher<Possible<T>>.map(
        ifSuccess: (T) -> O,
        ifError: (Exception) -> O,
        ifUndefined: () -> O
) =
        map { possibleData -> possibleData.handle(ifSuccess, ifError, ifUndefined) }

fun <T : Any, O : Any> Finisher<Possible<T>>.map(
        ifSuccess: (T) -> O,
        ifError: (Exception?) -> O
) =
        map { possibleData -> possibleData.handle(ifSuccess, ifError) }

fun <T : Any, O : Any> Finisher<Possible<T>>.map(
        forSuccess: O,
        forError: O,
        forUndefined: O
) =
        map { possibleData -> possibleData.handle(forSuccess, forError, forUndefined) }

fun <T : Any, O : Any> Finisher<Possible<T>>.map(
        forSuccess: O,
        forError: O
) =
        map { possibleData -> possibleData.handle(forSuccess, forError) }

fun <T : Any> Finisher<Possible<T>>.mapPossibleToUnit() =
        mapPossible(Unit)

fun <T : Any, O : Any> Finisher<Possible<T>>.tryMapPossible(
        throwableConverter: (T) -> O
) =
        Finisher<Possible<O>> { onFinished ->
            await { data ->
                val convertedData = data.tryMap(throwableConverter)
                onFinished.invoke(convertedData)
            }
        }


fun <T : Any, O : Any> Finisher<Possible<T>>.mapPossibleAsync(
        additionalAction: (data: T, callback: (O) -> Unit) -> Unit
) =
        Finisher<Possible<O>> { onFinished ->
            await { finisherResult ->
                finisherResult.handle(
                        onSuccess = { additionalAction.invoke(it, onFinished::success) },
                        onError = { onFinished.error(it) },
                        onUndefined = onFinished::undefined
                )
            }
        }


fun <T : Any, O : Any> Finisher<Possible<T>>.tryMapPossibleAsync(
        throwsAdditionalAction: (data: T, callback: (O) -> Unit) -> Unit
) =
        Finisher<Possible<O>> { onFinished ->
            await { finisherResult ->
                finisherResult.handle(
                        onSuccess = {
                            tryOrElse(
                                    throwsAction = {
                                        throwsAdditionalAction.invoke(it, onFinished::success)
                                    },
                                    onThrow = { ex ->
                                        onFinished.error(ex as? Exception)
                                    }
                            )
                        },
                        onError = { onFinished.error(it) },
                        onUndefined = onFinished::undefined
                )
            }
        }


fun <T : Any, O : Any> Finisher<Possible<T>>.mapPossibleOrError(
        converter: (T) -> Possible<O>
) =
        Finisher<Possible<O>> { onFinished ->
            await { data ->
                val convertedData = data.mapPossible(converter)
                onFinished.invoke(convertedData)
            }
        }

fun <T : Any, O : Any> Finisher<Possible<T>>.mapPossibleOrError(possibleData: Possible<O>) =
        mapPossibleOrError { _ -> possibleData }


fun <T : Any, O : Any> Finisher<Possible<T>>.tryMapPossibleOrError(
        throwsConverter: (T) -> Possible<O>
) =
        Finisher<Possible<O>> { onFinished ->
            await { data ->
                val convertedData = data.tryMapPossible(throwsConverter)
                onFinished.invoke(convertedData)
            }
        }


fun <T : Any, O : Any> Finisher<Possible<T>>.mapPossibleOrErrorAsync(
        additionalAction: (data: T, callback: (Possible<O>) -> Unit) -> Unit
) =
        Finisher<Possible<O>> { onFinished ->
            await { finisherResult ->
                finisherResult.handle(
                        onSuccess = { additionalAction.invoke(it, onFinished) },
                        onError = { onFinished.error(it) },
                        onUndefined = onFinished::undefined
                )
            }
        }


fun <T : Any, O : Any> Finisher<Possible<T>>.tryMapPossibleOrErrorAsync(
        throwsAdditionalAction: (data: T, callback: (Possible<O>) -> Unit) -> Unit
) =
        Finisher<Possible<O>> { onFinished ->
            await { finisherResult ->
                finisherResult.handle(
                        onSuccess = {
                            tryOrElse(
                                    throwsAction = {
                                        throwsAdditionalAction.invoke(it, onFinished)
                                    },
                                    onThrow = { ex ->
                                        onFinished.error(ex as? Exception)
                                    }
                            )
                        },
                        onError = { onFinished.error(it) },
                        onUndefined = onFinished::undefined
                )
            }
        }