package ru.hnau.jutils.finisher

import kotlin.concurrent.thread


@Deprecated("Use kotlinx.coroutines.Deferred instead")
class NewThreadFinisher<T>(
        action: (onFinished: (T) -> Unit) -> Unit
) : Finisher<T>(
        { onFinished -> thread { action.invoke(onFinished) } }
) {

    companion object {

        fun <T> sync(action: () -> T) =
                NewThreadFinisher<T> { onFinished ->
                    val result = action.invoke()
                    onFinished.invoke(result)
                }

    }

}