package ru.hnau.jutils.collections.array


inline fun ShortArray.findPos(predicate: (Short) -> Boolean): Int? {
    forEachIndexed { pos, element -> if (predicate.invoke(element)) return pos }
    return null
}

inline fun <R> ShortArray.convertFirstNotNull(converter: (Short) -> R?): R? {
    forEach { item ->
        val convertionResult = converter.invoke(item)
        if (convertionResult != null) {
            return convertionResult
        }
    }
    return null
}

fun ShortArray.circledGet(pos: Int): Short {
    val size = this.size
    val normalizedPos = (pos % size).let { if (it < 0) it + size else it }
    return get(normalizedPos)
}

//Для случая, когда список может быть пустым
fun ShortArray.circledGetOrNull(pos: Int) =
        if (this.isEmpty()) null else circledGet(pos)

fun ShortArray.getOrFirst(pos: Int): Short = getOrNull(pos) ?: get(0)

fun ShortArray.getOrFirstOrNull(pos: Int): Short? =
        if (this.isEmpty()) null else getOrNull(pos)

inline fun ShortArray.sumByByte(selector: (Short) -> Byte): Byte {
    var sum: Byte = 0
    for (element in this) {
        sum = (sum + selector.invoke(element)).toByte()
    }
    return sum
}

inline fun ShortArray.sumByShort(selector: (Short) -> Short): Short {
    var sum: Short = 0
    for (element in this) {
        sum = (sum + selector.invoke(element)).toShort()
    }
    return sum
}

inline fun ShortArray.sumByLong(selector: (Short) -> Long): Long {
    var sum: Long = 0
    for (element in this) {
        sum += selector.invoke(element)
    }
    return sum
}

inline fun ShortArray.sumByDouble(selector: (Short) -> Double): Double {
    var sum = 0.0
    for (element in this) {
        sum += selector.invoke(element)
    }
    return sum
}

inline fun ShortArray.sumByFloat(selector: (Short) -> Float): Float {
    var sum = 0f
    for (element in this) {
        sum += selector.invoke(element)
    }
    return sum
}

inline fun ShortArray.sumByString(selector: (Short) -> String): String {
    val result = StringBuilder()
    for (element in this) {
        result.append(selector.invoke(element))
    }
    return result.toString()
}