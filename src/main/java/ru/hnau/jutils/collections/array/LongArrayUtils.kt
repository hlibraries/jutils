package ru.hnau.jutils.collections.array


inline fun LongArray.findPos(predicate: (Long) -> Boolean): Int? {
    forEachIndexed { pos, element -> if (predicate.invoke(element)) return pos }
    return null
}

inline fun <R> LongArray.convertFirstNotNull(converter: (Long) -> R?): R? {
    forEach { item ->
        val convertionResult = converter.invoke(item)
        if (convertionResult != null) {
            return convertionResult
        }
    }
    return null
}

fun LongArray.circledGet(pos: Int): Long {
    val size = this.size
    val normalizedPos = (pos % size).let { if (it < 0) it + size else it }
    return get(normalizedPos)
}

//Для случая, когда список может быть пустым
fun LongArray.circledGetOrNull(pos: Int) =
        if (this.isEmpty()) null else circledGet(pos)

fun LongArray.getOrFirst(pos: Int): Long = getOrNull(pos) ?: get(0)

fun LongArray.getOrFirstOrNull(pos: Int): Long? =
        if (this.isEmpty()) null else getOrNull(pos)

inline fun LongArray.sumByByte(selector: (Long) -> Byte): Byte {
    var sum: Byte = 0
    for (element in this) {
        sum = (sum + selector.invoke(element)).toByte()
    }
    return sum
}

inline fun LongArray.sumByShort(selector: (Long) -> Short): Short {
    var sum: Short = 0
    for (element in this) {
        sum = (sum + selector.invoke(element)).toShort()
    }
    return sum
}

inline fun LongArray.sumByLong(selector: (Long) -> Long): Long {
    var sum: Long = 0
    for (element in this) {
        sum += selector.invoke(element)
    }
    return sum
}

inline fun LongArray.sumByDouble(selector: (Long) -> Double): Double {
    var sum = 0.0
    for (element in this) {
        sum += selector.invoke(element)
    }
    return sum
}

inline fun LongArray.sumByFloat(selector: (Long) -> Float): Float {
    var sum = 0f
    for (element in this) {
        sum += selector.invoke(element)
    }
    return sum
}

inline fun LongArray.sumByString(selector: (Long) -> String): String {
    val result = StringBuilder()
    for (element in this) {
        result.append(selector.invoke(element))
    }
    return result.toString()
}