package ru.hnau.jutils.collections.array


inline fun IntArray.findPos(predicate: (Int) -> Boolean): Int? {
    forEachIndexed { pos, element -> if (predicate.invoke(element)) return pos }
    return null
}

inline fun <R> IntArray.convertFirstNotNull(converter: (Int) -> R?): R? {
    forEach { item ->
        val convertionResult = converter.invoke(item)
        if (convertionResult != null) {
            return convertionResult
        }
    }
    return null
}

fun IntArray.circledGet(pos: Int): Int {
    val size = this.size
    val normalizedPos = (pos % size).let { if (it < 0) it + size else it }
    return get(normalizedPos)
}

//Для случая, когда список может быть пустым
fun IntArray.circledGetOrNull(pos: Int) =
        if (this.isEmpty()) null else circledGet(pos)

fun IntArray.getOrFirst(pos: Int): Int = getOrNull(pos) ?: get(0)

fun IntArray.getOrFirstOrNull(pos: Int): Int? =
        if (this.isEmpty()) null else getOrNull(pos)

inline fun IntArray.sumByByte(selector: (Int) -> Byte): Byte {
    var sum: Byte = 0
    for (element in this) {
        sum = (sum + selector.invoke(element)).toByte()
    }
    return sum
}

inline fun IntArray.sumByShort(selector: (Int) -> Short): Short {
    var sum: Short = 0
    for (element in this) {
        sum = (sum + selector.invoke(element)).toShort()
    }
    return sum
}

inline fun IntArray.sumByLong(selector: (Int) -> Long): Long {
    var sum: Long = 0
    for (element in this) {
        sum += selector.invoke(element)
    }
    return sum
}

inline fun IntArray.sumByDouble(selector: (Int) -> Double): Double {
    var sum = 0.0
    for (element in this) {
        sum += selector.invoke(element)
    }
    return sum
}

inline fun IntArray.sumByFloat(selector: (Int) -> Float): Float {
    var sum = 0f
    for (element in this) {
        sum += selector.invoke(element)
    }
    return sum
}

inline fun IntArray.sumByString(selector: (Int) -> String): String {
    val result = StringBuilder()
    for (element in this) {
        result.append(selector.invoke(element))
    }
    return result.toString()
}