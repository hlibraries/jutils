package ru.hnau.jutils.collections.array


inline fun <T> Array<T>.findPos(predicate: (T) -> Boolean): Int? {
    forEachIndexed { pos, element -> if (predicate.invoke(element)) return pos }
    return null
}

fun <T> Array<T>.firstNotNull() = find { it != null }

inline fun <T, R> Array<T>.convertFirstNotNull(converter: (T) -> R?): R? {
    forEach { item ->
        val convertionResult = converter.invoke(item)
        if (convertionResult != null) {
            return convertionResult
        }
    }
    return null
}

fun <T> Array<T>.circledGet(pos: Int): T {
    val size = this.size
    val normalizedPos = (pos % size).let { if (it < 0) it + size else it }
    return get(normalizedPos)
}

//Для случая, когда список может быть пустым
fun <T> Array<T>.circledGetOrNull(pos: Int) =
        if (this.isEmpty()) null else circledGet(pos)

fun <T> Array<T>.getOrFirst(pos: Int): T = getOrNull(pos) ?: get(0)

fun <T> Array<T>.getOrFirstOrNull(pos: Int): T? =
        if (this.isEmpty()) null else getOrNull(pos)

inline fun <T> Array<T>.sumByByte(selector: (T) -> Byte): Byte {
    var sum: Byte = 0
    for (element in this) {
        sum = (sum + selector.invoke(element)).toByte()
    }
    return sum
}

inline fun <T> Array<T>.sumByShort(selector: (T) -> Short): Short {
    var sum: Short = 0
    for (element in this) {
        sum = (sum + selector.invoke(element)).toShort()
    }
    return sum
}

inline fun <T> Array<T>.sumByLong(selector: (T) -> Long): Long {
    var sum: Long = 0
    for (element in this) {
        sum += selector.invoke(element)
    }
    return sum
}

inline fun <T> Array<T>.sumByDouble(selector: (T) -> Double): Double {
    var sum = 0.0
    for (element in this) {
        sum += selector.invoke(element)
    }
    return sum
}

inline fun <T> Array<T>.sumByFloat(selector: (T) -> Float): Float {
    var sum = 0f
    for (element in this) {
        sum += selector.invoke(element)
    }
    return sum
}

inline fun <T> Array<T>.sumByString(selector: (T) -> String): String {
    val result = StringBuilder()
    for (element in this) {
        result.append(selector.invoke(element))
    }
    return result.toString()
}