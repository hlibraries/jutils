package ru.hnau.jutils.collections.array


inline fun FloatArray.findPos(predicate: (Float) -> Boolean): Int? {
    forEachIndexed { pos, element -> if (predicate.invoke(element)) return pos }
    return null
}

inline fun <R> FloatArray.convertFirstNotNull(converter: (Float) -> R?): R? {
    forEach { item ->
        val convertionResult = converter.invoke(item)
        if (convertionResult != null) {
            return convertionResult
        }
    }
    return null
}

fun FloatArray.circledGet(pos: Int): Float {
    val size = this.size
    val normalizedPos = (pos % size).let { if (it < 0) it + size else it }
    return get(normalizedPos)
}

//Для случая, когда список может быть пустым
fun FloatArray.circledGetOrNull(pos: Int) =
        if (this.isEmpty()) null else circledGet(pos)

fun FloatArray.getOrFirst(pos: Int): Float = getOrNull(pos) ?: get(0)

fun FloatArray.getOrFirstOrNull(pos: Int): Float? =
        if (this.isEmpty()) null else getOrNull(pos)

inline fun FloatArray.sumByByte(selector: (Float) -> Byte): Byte {
    var sum: Byte = 0
    for (element in this) {
        sum = (sum + selector.invoke(element)).toByte()
    }
    return sum
}

inline fun FloatArray.sumByShort(selector: (Float) -> Short): Short {
    var sum: Short = 0
    for (element in this) {
        sum = (sum + selector.invoke(element)).toShort()
    }
    return sum
}

inline fun FloatArray.sumByLong(selector: (Float) -> Long): Long {
    var sum: Long = 0
    for (element in this) {
        sum += selector.invoke(element)
    }
    return sum
}

inline fun FloatArray.sumByDouble(selector: (Float) -> Double): Double {
    var sum = 0.0
    for (element in this) {
        sum += selector.invoke(element)
    }
    return sum
}

inline fun FloatArray.sumByFloat(selector: (Float) -> Float): Float {
    var sum = 0f
    for (element in this) {
        sum += selector.invoke(element)
    }
    return sum
}

inline fun FloatArray.sumByString(selector: (Float) -> String): String {
    val result = StringBuilder()
    for (element in this) {
        result.append(selector.invoke(element))
    }
    return result.toString()
}