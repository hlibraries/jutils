package ru.hnau.jutils.collections.array


inline fun DoubleArray.findPos(predicate: (Double) -> Boolean): Int? {
    forEachIndexed { pos, element -> if (predicate.invoke(element)) return pos }
    return null
}

inline fun <R> DoubleArray.convertFirstNotNull(converter: (Double) -> R?): R? {
    forEach { item ->
        val convertionResult = converter.invoke(item)
        if (convertionResult != null) {
            return convertionResult
        }
    }
    return null
}

fun DoubleArray.circledGet(pos: Int): Double {
    val size = this.size
    val normalizedPos = (pos % size).let { if (it < 0) it + size else it }
    return get(normalizedPos)
}

//Для случая, когда список может быть пустым
fun DoubleArray.circledGetOrNull(pos: Int) =
        if (this.isEmpty()) null else circledGet(pos)

fun DoubleArray.getOrFirst(pos: Int): Double = getOrNull(pos) ?: get(0)

fun DoubleArray.getOrFirstOrNull(pos: Int): Double? =
        if (this.isEmpty()) null else getOrNull(pos)

inline fun DoubleArray.sumByByte(selector: (Double) -> Byte): Byte {
    var sum: Byte = 0
    for (element in this) {
        sum = (sum + selector.invoke(element)).toByte()
    }
    return sum
}

inline fun DoubleArray.sumByShort(selector: (Double) -> Short): Short {
    var sum: Short = 0
    for (element in this) {
        sum = (sum + selector.invoke(element)).toShort()
    }
    return sum
}

inline fun DoubleArray.sumByLong(selector: (Double) -> Long): Long {
    var sum: Long = 0
    for (element in this) {
        sum += selector.invoke(element)
    }
    return sum
}

inline fun DoubleArray.sumByDouble(selector: (Double) -> Double): Double {
    var sum = 0.0
    for (element in this) {
        sum += selector.invoke(element)
    }
    return sum
}

inline fun DoubleArray.sumByFloat(selector: (Double) -> Float): Float {
    var sum = 0f
    for (element in this) {
        sum += selector.invoke(element)
    }
    return sum
}

inline fun DoubleArray.sumByString(selector: (Double) -> String): String {
    val result = StringBuilder()
    for (element in this) {
        result.append(selector.invoke(element))
    }
    return result.toString()
}