package ru.hnau.jutils.collections




inline fun <T> MutableCollection<T>.findAndRemove(predicate: (T) -> Boolean): T? {
    forEach {
        if (predicate.invoke(it)) {
            remove(it)
            return it
        }
    }
    return null
}
