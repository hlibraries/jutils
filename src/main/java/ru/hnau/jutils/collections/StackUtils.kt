package ru.hnau.jutils.collections

import java.util.*

fun <T> Stack<T>.popOrNull() = synchronized(this) { if (empty()) null else pop() }

fun <T> Stack<T>.peekOrNull() = synchronized(this) { if (empty()) null else peek() }