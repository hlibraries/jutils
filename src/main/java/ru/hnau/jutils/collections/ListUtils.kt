package ru.hnau.jutils.collections


fun <T> List<T>.circledGet(pos: Int): T {
    val size = this.size
    val normalizedPos = (pos % size).let { if (it < 0) it + size else it }
    return get(normalizedPos)
}

//Для случая, когда список может быть пустым
fun <T> List<T>.circledGetOrNull(pos: Int) =
        if (this.isEmpty()) null else circledGet(pos)

fun <T> List<T>.getOrFirst(pos: Int): T = getOrNull(pos) ?: get(0)

fun <T> List<T>.getOrFirstOrNull(pos: Int): T? =
        if (this.isEmpty()) null else getOrNull(pos)