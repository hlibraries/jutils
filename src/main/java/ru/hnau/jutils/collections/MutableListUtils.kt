package ru.hnau.jutils.collections


fun <T> MutableList<T>.setSize(newSize: Int, emptyElement: T) {
    val oldSize = size
    val countDelta = newSize - oldSize

    when {
        countDelta > 0 -> repeat(countDelta) { add(emptyElement) }
        countDelta < 0 -> {
            dropLast(-countDelta)
        }
    }
}