package ru.hnau.jutils.collections


inline fun <T> Iterable<T>.findPos(predicate: (T) -> Boolean): Int? {
    forEachIndexed { pos, element -> if (predicate.invoke(element)) return pos }
    return null
}

fun <T> Iterable<T>.firstNotNull() = find { it != null }

inline fun <T, R> Iterable<T>.firstNotNullConvertion(converter: (T) -> R?): R? {
    forEach { item ->
        val convertionResult = converter.invoke(item)
        if (convertionResult != null) {
            return convertionResult
        }
    }
    return null
}

inline fun <T> Iterable<T>.sumByByte(selector: (T) -> Byte): Byte {
    var sum: Byte = 0
    for (element in this) {
        sum = (sum + selector.invoke(element)).toByte()
    }
    return sum
}

inline fun <T> Iterable<T>.sumByShort(selector: (T) -> Short): Short {
    var sum: Short = 0
    for (element in this) {
        sum = (sum + selector.invoke(element)).toShort()
    }
    return sum
}

inline fun <T> Iterable<T>.sumByLong(selector: (T) -> Long): Long {
    var sum: Long = 0
    for (element in this) {
        sum += selector.invoke(element)
    }
    return sum
}

inline fun <T> Iterable<T>.sumByDouble(selector: (T) -> Double): Double {
    var sum = 0.0
    for (element in this) {
        sum += selector.invoke(element)
    }
    return sum
}

inline fun <T> Iterable<T>.sumByFloat(selector: (T) -> Float): Float {
    var sum = 0f
    for (element in this) {
        sum += selector.invoke(element)
    }
    return sum
}

inline fun <T> Iterable<T>.sumByString(selector: (T) -> String): String {
    val result = StringBuilder()
    for (element in this) {
        result.append(selector.invoke(element))
    }
    return result.toString()
}

fun <T> Iterable<T>.minusAt(position: Int) =
        filterIndexed { index, _ -> index != position }

inline fun <T> Iterable<T>.minusFirst(predicate: (T) -> Boolean): List<T> {
    var dropped = false
    var result = ArrayList<T>(count())
    for (element in this) {
        if (dropped || !predicate(element)) {
            result.add(element)
        } else {
            dropped = true
        }
    }
    return result
}