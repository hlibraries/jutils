package ru.hnau.jutils.helpers


abstract class Holder<T> {

    private val locks = LinkedHashSet<T>()

    fun hold(lock: T) = synchronized(locks) {
        val added = locks.add(lock)
        if (added && locks.size == 1) {
            onFirstHolt()
        }
    }

    fun release(lock: T) = synchronized(this) {
        val removed = locks.remove(lock)
        if (removed && locks.isEmpty()) {
            onLastReleased()
        }
    }

    protected abstract fun onFirstHolt()

    protected abstract fun onLastReleased()

}