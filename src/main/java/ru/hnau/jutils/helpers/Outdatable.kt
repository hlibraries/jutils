package ru.hnau.jutils.helpers

import ru.hnau.jutils.TimeValue


class Outdatable<T : Any>(
        value: T,
        private val lifetime: TimeValue? = null
) {

    private val outdatableValue = value
    private val valueReceived = TimeValue.now()

    val value: T?
        get() = outdatableValue.takeIf { valueReceived.isActual(lifetime) }

}

fun <T : Any> T.toOutdatable(lifetime: TimeValue? = null) =
        Outdatable(this, lifetime)