package ru.hnau.jutils.helpers.weak


class WeakListener<T>(
        private val onReferenceEmpty: ((WeakListener<T>) -> Unit)? = null,
        innerListener: (T) -> Unit
) : (T) -> Unit {

    private val innerListenerReference = Weak(innerListener)

    override fun invoke(value: T) = synchronized(this) {
        val innerListener = innerListenerReference.value
        if (innerListener == null) {
            onReferenceEmpty?.invoke(this@WeakListener)
            return@synchronized
        }
        innerListener.invoke(value)
    }

}