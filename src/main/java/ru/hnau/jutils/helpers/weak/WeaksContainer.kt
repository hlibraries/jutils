package ru.hnau.jutils.helpers.weak

import java.util.*


open class WeaksContainer<T> : MutableIterable<T> {

    private val content = LinkedList<Weak<T>>()

    fun add(item: T) =
            content.add(Weak(item))

    override fun iterator() = object : MutableIterator<T> {

        private val innerIterator = content.iterator()

        private var next: T? = null

        init {
            updateNext()
        }

        private tailrec fun updateNext() {

            if (!innerIterator.hasNext()) {
                next = null
                return
            }

            val value = innerIterator.next().value
            if (value != null) {
                next = value
                return
            }

            innerIterator.remove()
            updateNext()

        }

        override fun hasNext() = next != null

        override fun next(): T {
            val next = this.next ?: throw IllegalStateException("No next element")
            updateNext()
            return next
        }

        override fun remove() {
            innerIterator.remove()
            updateNext()
        }

    }

}