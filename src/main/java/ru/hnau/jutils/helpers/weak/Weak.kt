package ru.hnau.jutils.helpers.weak

import java.lang.ref.WeakReference


class Weak<T>(value: T) : () -> T? {

    private val valueWeakReference = WeakReference(value)

    val value: T?
        get() = valueWeakReference.get()

    override fun invoke() = value

}