package ru.hnau.jutils.helpers

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Job
import ru.hnau.jutils.asException
import ru.hnau.jutils.coroutines.deferred.result
import ru.hnau.jutils.handle
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.possible.error
import ru.hnau.jutils.possible.success


interface Completable<T> {

    fun executeOnCompleted(onCompletedListener: (T) -> Unit)

}

open class NeverCompletedCompletable<T> : Completable<T> {

    override fun executeOnCompleted(onCompletedListener: (T) -> Unit) {}

}

object NeverCompletedUnitCompletable : NeverCompletedCompletable<Unit>()

val Job.asCompletable: Completable<Unit>
    get() = object : Completable<Unit> {
        override fun executeOnCompleted(onCompletedListener: (Unit) -> Unit) {
            invokeOnCompletion { onCompletedListener.invoke(Unit) }
        }
    }

val <T : Any> Deferred<T>.asCompletable: Completable<Possible<T>>
    get() = object : Completable<Possible<T>> {
        override fun executeOnCompleted(onCompletedListener: (Possible<T>) -> Unit) {
            invokeOnCompletion { th ->
                th.handle(
                        ifNotNull = { onCompletedListener.error(it.asException) },
                        ifNull = { onCompletedListener.success(result) }
                )
            }
        }
    }