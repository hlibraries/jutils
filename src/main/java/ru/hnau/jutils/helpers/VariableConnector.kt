package ru.hnau.jutils.helpers


class VariableConnector<T>(
        private val getter: () -> T,
        private val setter: (T) -> Unit
) {

    var value: T
        set(value) {
            setter.invoke(value)
        }
        get() = getter.invoke()

}