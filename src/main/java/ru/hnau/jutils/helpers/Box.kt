package ru.hnau.jutils.helpers


data class Box<T>(val value: T)

inline fun <T, R : Any> Box<T>?.ifExists(action: (T) -> R) =
        this?.let { action.invoke(value) }

fun <T> T.toBox() = Box(this)