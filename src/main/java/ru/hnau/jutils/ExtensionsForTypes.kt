package ru.hnau.jutils

import java.io.PrintWriter
import java.io.StringWriter

fun Int.toBoolean() = this != 0
fun Long.toBoolean() = this != 0L
fun Short.toBoolean() = toInt().toBoolean()
fun Byte.toBoolean() = toInt().toBoolean()
fun Float.toBoolean() = this != 0f
fun Double.toBoolean() = this != 0.0

fun <T : Any> T.toSingleItemList() = listOf(this)
fun <T : Any> T?.toSingleItemOrEmptyList() = listOfNotNull(this)

val Throwable.stringStackTrace: String
    get() {
        val stringWriter = StringWriter()
        printStackTrace(PrintWriter(stringWriter))
        return stringWriter.toString()
    }

val <T> T.me: T
    get() = this